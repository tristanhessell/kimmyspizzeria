package tristan.hessell.pizzagame.app.recipe;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import tristan.hessell.pizzagame.app.R;
import tristan.hessell.pizzagame.app.ingredient.INGREDIENT;
import tristan.hessell.pizzagame.app.pizza.Pizza;
import tristan.hessell.pizzagame.app.pizza.PizzaFactory;

import java.util.List;

/**
 * Representation of a pizza recipe.
 * Created by Tristan on 20/05/2015.
 */
public class RecipeView extends LinearLayout
{
    private ImageView imPizza;
    private TextView tvName;
    private ImageView imCapsicum;
    private ImageView imHam;
    private ImageView imMushroom;
    private ImageView imOlives;
    private ImageView imOnion;
    private ImageView imPepperoni;
    private ImageView imPineapple;

    private final static float INGREDIENT_NOT_USED_ALPHA = 0.25F;
    private final static float INGREDIENT_USED_ALPHA = 1.0F;

    /**
     * XML constructor.
     * <p/>
     * Requires app:pizza="" to work.
     *
     * @param context The parent of the RecipeView object
     * @param attrs   attributes imported via XML
     */
    public RecipeView( final Context context, final AttributeSet attrs )
    {
        super( context );
        ((LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE ))
                .inflate( R.layout.recipeview, this, true );

        TypedArray a = context.obtainStyledAttributes( attrs,
                R.styleable.RecipeView, 0, 0 );

        initialise( PizzaFactory.getPizza( a.getString( R.styleable.RecipeView_pizza ).toUpperCase() ) );
        a.recycle();
    }

    /**
     * Programmatic constructor.
     *
     * @param context The parent of the RecipeView object
     * @param pizz The pizza that this view represents
     */
    public RecipeView( final Context context, final Pizza pizz )
    {
        super( context );
        ((LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE ))
                .inflate( R.layout.recipeview, this, true );

        initialise( pizz );
    }

    /**
     * Function that is called during construction.
     * Finds the various GUI components and sets the images of the pizza & ingredients.
     *
     * @param pizz The pizza to load into this Recipe
     */
    private void initialise( final Pizza pizz )
    {
        imPizza = (ImageView) findViewById( R.id.imPizza );
        tvName = (TextView) findViewById( R.id.tvName );
        imCapsicum = (ImageView) findViewById( R.id.imCapsicum );
        imHam = (ImageView) findViewById( R.id.imHam );
        imMushroom = (ImageView) findViewById( R.id.imMushroom );
        imOlives = (ImageView) findViewById( R.id.imOlives );
        imOnion = (ImageView) findViewById( R.id.imOnion );
        imPepperoni = (ImageView) findViewById( R.id.imPepperoni );
        imPineapple = (ImageView) findViewById( R.id.imPineapple );
        setPizza( pizz );
    }

    /**
     * Used to change the content of the view.
     * Givent the pizza parameter, the views pizza image, ingredients and name are changed.
     * @param pizz The pizza to load into this Recipe
     */
    public final void setPizza( final Pizza pizz )
    {
        imPizza.setImageResource( pizz.getImageResourceId() );
        updateIngredientImages( pizz );
        tvName.setText( pizz.getName() );
    }

    /**
     * Updates the opacity of the ingredients - semi-transparent ingredients are not used in the recipe.
     */
    private void updateIngredientImages( final Pizza pizz)
    {
        imCapsicum.setAlpha( INGREDIENT_NOT_USED_ALPHA );
        imHam.setAlpha( INGREDIENT_NOT_USED_ALPHA );
        imMushroom.setAlpha( INGREDIENT_NOT_USED_ALPHA );
        imOlives.setAlpha( INGREDIENT_NOT_USED_ALPHA );
        imOnion.setAlpha( INGREDIENT_NOT_USED_ALPHA );
        imPepperoni.setAlpha( INGREDIENT_NOT_USED_ALPHA );
        imPineapple.setAlpha( INGREDIENT_NOT_USED_ALPHA );

        List<INGREDIENT> ingredients = pizz.getIngredients();

        for( INGREDIENT ing : ingredients )
        {
            switch( ing )
            {
                case CHEESE:
                    break;
                case HAM:
                    imHam.setAlpha( INGREDIENT_USED_ALPHA );
                    break;
                case PINEAPPLE:
                    imPineapple.setAlpha( INGREDIENT_USED_ALPHA );
                    break;
                case MUSHROOM:
                    imMushroom.setAlpha( INGREDIENT_USED_ALPHA );
                    break;
                case ONION:
                    imOnion.setAlpha( INGREDIENT_USED_ALPHA );
                    break;
                case CAPSICUM:
                    imCapsicum.setAlpha( INGREDIENT_USED_ALPHA );
                    break;
                case PEPPERONI:
                    imPepperoni.setAlpha( INGREDIENT_USED_ALPHA );
                    break;
                case OLIVES:
                    imOlives.setAlpha( INGREDIENT_USED_ALPHA );
                    break;
                case TOMATOSAUCE:
                    break;
                default:
                    System.out.println( "Cannot change ingredient - have you added more recently?");
            }
        }
    }
}