package tristan.hessell.pizzagame.app;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import tristan.hessell.pizzagame.app.ingredient.INGREDIENT;
import tristan.hessell.pizzagame.app.pizza.Pizza;

import java.util.Collection;
import java.util.List;

/**
 * Represents the ordered pizza. Contains a Pizza with its name next to the right.
 * The use of View in the name is misleading - as it is actually a Linear Layout
 *
 * Created by Tristan on 21/04/2015.
 */
public final class OrderedPizzaView extends LinearLayout
{
    private ImageView imgView;
    private TextView tvName;
    //The handler is required to tell the main activity to remove this order or not (when clicked)
    private Handler handler;
    private Pizza thisPizza;

    /**
     * XML Constructor
     *
     * @param context The parent that this class resides in.
     * @param attrs   The attributes interpreted from the XML declaration
     */
    public OrderedPizzaView( final Context context, final AttributeSet attrs )
    {
        super( context, attrs );
        ((LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE ))
                .inflate( R.layout.orderpizzaview, this, true );

        initialise(); //get the GUI components
    }

    /**
     * Programmatic constructor
     *
     * @param context The parent of this class
     */
    public OrderedPizzaView( final Context context, final Pizza pizz, final Handler handle )
    {
        super( context );
        ((LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE ))
                .inflate( R.layout.orderpizzaview, this, true );

        initialise(); //get the GUI components
        initialise( pizz, handle ); //set those GUI components
    }

    /**
     * Called by the constructors to set up the various View components that comprise this
     * element.
     *
     * Need an empty intialise for when the element is declared in XML -
     * theres no way to get the handler from XML.
     *
     * Could be named better though ...
     */
    private void initialise()
    {
        imgView = (ImageView) findViewById( R.id.ivPizza );
        tvName = (TextView) findViewById( R.id.tvPizzaName );
        setOnClickListener( new OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                handler.obtainMessage( MainActivity.ORDER_FULFILLED, 1, 0, OrderedPizzaView.this ).sendToTarget();
            }
        } );
    }

    /**
     * Called to set content of the various views.
     * Pizza object is used to simplify the function interface (rather than having a name & ingredients).
     * Also as assurance of consistency
     *
     * @param piz       The pizza that this Order represents
     * @param inHandler The handler from the parent that this order contacts
     */
    public final void initialise( final Pizza piz, final Handler inHandler )
    {
        thisPizza = piz;
        handler = inHandler;
        imgView.setImageResource( thisPizza.getImageResourceId() );
        tvName.setText( thisPizza.getName() );
    }

    /**
     * Checks whether the specified ingredients matches those of the pizza this represents
     *
     * @param inIngredients List of ingredients that the user wants to check
     * @return true if the igredients match, otherwise false
     */
    public final boolean ingredientsMatch( final Collection<INGREDIENT> inIngredients )
    {
        return thisPizza.ingredientsCorrect( inIngredients );
    }

    /**
     * Returns the name of the pizza order.
     * Used only to save the order when the game is paused etc.
     *
     * @return String representing the pizza
     */
    public final String getPizzaName()
    {
        return thisPizza.getName();
    }
}
