package tristan.hessell.pizzagame.app.pizza;

import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import tristan.hessell.pizzagame.app.ingredient.INGREDIENT;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Factory class responsible for reading in the pizza data from a file
 * and then creating a random pizza throughout the game.
 * <p/>
 * A set appears more suitable than a list as there should only be one kind of each pizza.
 * However, the random access of getting a random pizza is the important functionality.
 * <p/>
 *
 * Not synchronized.
 *
 * Created by Tristan on 19/04/2015.
 */
public class PizzaFactory
{
    private static final List<Pizza> pizzas = new ArrayList<>();

    /**
     * Loads the pizzas from the file into this class.
     *
     * @param reader InputStream representing the file of pizzas (in XML)
     */
    public static void loadPizzas( final InputStream reader )
    {
        if( pizzas.isEmpty() )
        {
            try
            {
                //Get each Pizza element from the file
                final List<Element> pizzasXML = new SAXBuilder().build( reader ).getRootElement().getChildren( "Pizza" );

                //Create a pizza object for each pizza element
                for( Element pizza : pizzasXML )
                {
                    //Get the name from the XML element
                    final String name = pizza.getChildText( "Name" );
                    //Get the ingredients from the XML element
                    final List<Element> ingredientsXML = pizza.getChild( "Ingredients" ).getChildren( "Ingredient" );
                    final List<INGREDIENT> ingredients = new ArrayList<>();

                    //Convert the XML ingredients into INGREDIENT enums
                    for( Element ingredient : ingredientsXML )
                    {
                        ingredients.add( INGREDIENT.valueOf( ingredient.getText().toUpperCase() ) );
                    }

                    //Create the pizza object and add it to the list
                    final Pizza pizz = new Pizza( name, ingredients );

                    //if the pizza is already on the list, this is wrong!
                    //but it doesn't throw the game off - so no exception, only logged
                    if( pizzas.contains( pizz ) )
                    {
                        System.out.println( "Duplicate pizza \"" + name + "\"" );
                    }

                    pizzas.add( pizz );
                }

                Collections.sort( pizzas );
            }
            catch( JDOMException | IOException e )
            {
                System.out.println( "Error with loading pizzas" );
            }
        }
    }

    /**
     * Returns the pizza object specified by the parameter
     *
     * @param pizzaStr String representing the pizza object to create
     * @return Pizza object specified by the parameter
     */
    public static Pizza getPizza( final String pizzaStr )
    {
        for( Pizza piz : pizzas )
        {
            if( piz.getName().toLowerCase().equals( pizzaStr.toLowerCase() ) )
            {
                return piz;
            }
        }

        //of you get here, the specified pizza is not valid
        throw new IllegalArgumentException( "invalid pizza name: cannot get pizza" );
    }

    /**
     * Gets a random pizza from the list
     *
     * @return Random Pizza object from the list
     */
    public static Pizza getRandomPizza()
    {
        return pizzas.get( (int) Math.floor( Math.random() * pizzas.size() ) );
    }

    /**
     * Returns the list of pizzas in the PizzaFactory
     *
     * @return List of pizzas from the PizzaFactory
     */
    public static List<Pizza> getAllPizzas()
    {
        return pizzas;
    }
}
