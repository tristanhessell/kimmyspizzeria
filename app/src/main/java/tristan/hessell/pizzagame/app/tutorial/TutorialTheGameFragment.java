package tristan.hessell.pizzagame.app.tutorial;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import tristan.hessell.pizzagame.app.R;

/**
 * The initial tutorial fragment - shows some simple text in the middle of the activity
 */
public class TutorialTheGameFragment extends ATutorialFragment
{
    public TutorialTheGameFragment()
    {}

    @Override
    public String getName()
    {
        return "The Game";
    }

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState )
    {
        View v = inflater.inflate( R.layout.fragment_the_game_tutorial, container, false );

        ((TextView) v.findViewById( R.id.tvContent )).setText( Html.fromHtml( getString( R.string.tutorial_introduction ) ) );

        return v;
    }
}
