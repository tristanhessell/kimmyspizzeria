package tristan.hessell.pizzagame.app.ingredient;

import android.os.Parcel;
import android.os.Parcelable;
import tristan.hessell.pizzagame.app.R;

/**
 * INGREDIENT enum is used to represent an ingredient that is used on a pizza.
 * Parcelable so the code in the main class looks a bit better.
 * enums are serializable, but serialization is an expensive process (although it probably wouldnt be
 * noticable at the small level that is used at in the app).
 *
 * Created by Tristan on 8/06/2015.
 */
public enum INGREDIENT implements Parcelable
{
    CHEESE( "Cheese", R.drawable.ingredient_cheese ),
    HAM( "Ham", R.drawable.ingredient_ham ),
    PINEAPPLE( "Pineapple", R.drawable.ingredient_pineapple ),
    MUSHROOM( "Mushroom", R.drawable.ingredient_mushroom ),
    TOMATOSAUCE( "Sauce", R.drawable.ingredient_tomatosauce ),
    ONION( "Onion", R.drawable.ingredient_onion ),
    CAPSICUM( "Capsicum", R.drawable.ingredient_capsicum ),
    PEPPERONI( "Pepperoni", R.drawable.ingredient_pepperoni ),
    OLIVES( "Olives", R.drawable.ingredient_olives );

    //the nice looking name for the ingredient
    public final String name;
    //the resource id for the image of the ingredient
    public final int resId;
    //used for a little bit of efficiency - calling .values() generetes a new array every time
    public final static INGREDIENT[] values = INGREDIENT.values();

    INGREDIENT( String inName, int inResId )
    {
        resId = inResId;
        name = inName;
    }


    /**
     * Required for parcelable implementation
     */
    @Override
    public int describeContents()
    {
        return 0;
    }

    /**
     * Required for parcelable implementation.
     * The position of the enum in its declaration is used to represent which enum this is.
     *
     * @param dest  the parcel which will contain the information about the enum
     * @param flags not used.
     */
    @Override
    public void writeToParcel( final Parcel dest, final int flags )
    {
        dest.writeInt( ordinal() );
    }

    /**
     * Required for parcelable implementation.
     */
    public static final Creator<INGREDIENT> CREATOR = new Creator<INGREDIENT>()
    {
        @Override
        public INGREDIENT createFromParcel( final Parcel source )
        {
            return INGREDIENT.values[source.readInt()];
        }

        @Override
        public INGREDIENT[] newArray( final int size )
        {
            return new INGREDIENT[size];
        }
    };
}

