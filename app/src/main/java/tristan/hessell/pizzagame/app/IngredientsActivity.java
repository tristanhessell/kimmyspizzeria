package tristan.hessell.pizzagame.app;

import android.app.Activity;
import android.os.Bundle;
import android.widget.GridView;
import tristan.hessell.pizzagame.app.ingredient.IngredientViewAdapter;

/**
 * Activity containing the ingredient page.
 * <p/>
 * Contains a title and a gridview - a simple activity.
 */
public class IngredientsActivity extends Activity
{
    @Override
    protected void onCreate( final Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_ingredients );
        GridView gridView = (GridView) findViewById( R.id.grid );
        gridView.setAdapter( new IngredientViewAdapter( this ) );
    }
}
