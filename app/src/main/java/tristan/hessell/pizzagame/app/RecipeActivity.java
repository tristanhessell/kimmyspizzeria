package tristan.hessell.pizzagame.app;

import android.app.Activity;
import android.os.Bundle;
import android.widget.GridView;
import tristan.hessell.pizzagame.app.recipe.RecipeAdapter;

/**
 * Contains a 2 by X grid of recipes along with their ingredients
 */
public class RecipeActivity extends Activity
{
    @Override
    protected void onCreate( final Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_recipe );
        final GridView gridView = (GridView) findViewById( R.id.grid );
        gridView.setAdapter( new RecipeAdapter( this ) );
    }
}
