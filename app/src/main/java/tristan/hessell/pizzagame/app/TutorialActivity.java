package tristan.hessell.pizzagame.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;
import com.viewpagerindicator.CirclePageIndicator;
import tristan.hessell.pizzagame.app.tutorial.*;

/**
 * Parent activity to the tutorial fragments
 */
public class TutorialActivity extends FragmentActivity
{
    private TextView tvTitle;

    private final ATutorialFragment[] fragments = new ATutorialFragment[]
            {
                new TutorialTheGameFragment(),
                new TutorialAddIngredientsFragment(),
                new TutorialSendOrdersFragment(),
                new TutorialDontForgetFragment()
            };

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_tutorial );

        tvTitle = (TextView) findViewById( R.id.tvTitle );
        tvTitle.setText( fragments[0].getName() );

        final ViewPager pager = (ViewPager) findViewById( R.id.pager );
        pager.setAdapter( new SliderPagerAdapter( getSupportFragmentManager() ) );
        pager.setPageTransformer( true, new ZoomOutPageTransformer() );

        final CirclePageIndicator circlePageIndicator = (CirclePageIndicator) findViewById( R.id.titles );
        circlePageIndicator.setOnPageChangeListener( new ViewPager.SimpleOnPageChangeListener()
        {
            @Override
            public void onPageSelected( int position )
            {
                tvTitle.setText( fragments[position].getName() );
            }
        } );
        circlePageIndicator.setViewPager( pager );
    }

    /**
     * Closes the activity.
     *
     * @param v Button
     */
    public void btnCloseClick( View v )
    {
        finish();
    }

    /**
     * Private inner implementation of the FragmentStatePagerAdatper abstract class.
     * Used by the Viewpager that allows the whole swiping views etc.
     */
    private class SliderPagerAdapter extends FragmentStatePagerAdapter
    {
        public SliderPagerAdapter( FragmentManager fm )
        {
            super( fm );
        }

        @Override
        public Fragment getItem( int position )
        {
            return fragments[position];
        }

        @Override
        public int getCount()
        {
            return fragments.length;
        }

    }
}
