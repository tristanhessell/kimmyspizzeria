package tristan.hessell.pizzagame.app.ingredient;

import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Shape that represents a quadrilateral drawn object.
 * <p/>
 * <p/>
 * Created by Tristan on 24/04/2015.
 */
public class Cheese extends PizzaShape
{
    //OGL ES contains the shaders you want to use
    private final int mProgram;
    private final FloatBuffer vertexBuffer;
    private final float color[];
    private static final int COORDS_PER_VERTEX = 3;
    private final int LINE_COUNT = 250;

    public Cheese( final float positionRadius, final float[] color )
    {
        super( new float[]{1, 0, 0, 0,
                           0, 1, 0, 0,
                           0, 0, 1, 0,
                           0, 0, 0, 1} );

        //the 2 is for because each line has two points - a start and a finish
        final float[] lineCoords = new float[LINE_COUNT * 2 * COORDS_PER_VERTEX];

        for( int ii = 0; ii < lineCoords.length; ii += 6 )
        {
            final float xPos = getRandomXPosition( positionRadius );
            final float yPos = getRandomYPosition( xPos, positionRadius );
            final float angle = getRandomRotation();
            final float sin = (float) Math.sin( angle * Math.PI / 2 );
            final float cos = (float) Math.cos( angle );

            lineCoords[ii] = xPos;
            lineCoords[ii + 1] = yPos;
            //lineCoords[ii+2] = 0F;
            lineCoords[ii + 3] = -0.1F * sin + xPos;
            lineCoords[ii + 4] = 0.1F * cos + yPos;
            //lineCoords[ii+5] = 0F;
        }

        this.color = color;

        // initialize vertex byte buffer for the line coordinates
        // (# of coordinate values * 4 bytes per float)
        final ByteBuffer bb = ByteBuffer.allocateDirect( lineCoords.length * 4 );
        bb.order( ByteOrder.nativeOrder() );
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put( lineCoords );
        vertexBuffer.position( 0 );

        // create empty OpenGL ES Program
        mProgram = GLES20.glCreateProgram();
        // add the vertex shader to program
        GLES20.glAttachShader( mProgram, vertexShader );
        // add the fragment shader to program
        GLES20.glAttachShader( mProgram, fragmentShader );
        // creates OpenGL ES program executables
        GLES20.glLinkProgram( mProgram );
    }

    @Override
    public void draw( final float[] mvpMatrix )
    {
        //add program to  OES environ
        GLES20.glUseProgram( mProgram );

        // get handle to programs vPosition member
        final int mPositionHandle = GLES20.glGetAttribLocation( mProgram, "vPosition" );

        //enable handle to the triangles verticies
        GLES20.glEnableVertexAttribArray( mPositionHandle );

        //prepare trianlge coord data
        final int vertexStride = COORDS_PER_VERTEX * 4;
        GLES20.glVertexAttribPointer( mPositionHandle, COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, vertexStride, vertexBuffer );

        // get the fragment shader vColor member
        final int mColorHandle = GLES20.glGetUniformLocation( mProgram, "vColor" );

        //set color for drawing the triangle
        GLES20.glUniform4fv( mColorHandle, 1, color, 0 );

        // get handle to shape's transformation matrix
        final int mMVPMatrixHandle = GLES20.glGetUniformLocation( mProgram, "uMVPMatrix" );

        // Pass the projection and view transformation to the shader
        GLES20.glUniformMatrix4fv( mMVPMatrixHandle, 1, false, mvpMatrix, 0 );

        //draw lines
        GLES20.glDrawArrays( GLES20.GL_LINES, 0, LINE_COUNT * 2 );

        //disable vertex array
        GLES20.glDisableVertexAttribArray( mPositionHandle );
    }

    /**
     * Cheese needs its own implementaion as each little piece is created here (and not in the renderer)
     *
     * @return returns a random float [0, 360]
     */
    private float getRandomRotation()
    {
        return (float) (Math.random() * Math.PI / 2) * 360.0F;
    }

    /**
     * Gets a randomly calculated x value.
     *
     * @param positionRadius The radius which the x value needs to be within.
     * @return value in range (-posRadius,posRadius).
     */
    private float getRandomXPosition( float positionRadius )
    {
        return (float) Math.random() * (2F * positionRadius) - positionRadius;
    }

    /**
     * Gets a randomly calculate y-value.
     * Calculated via the eqn of a circle ( rad^2 = x^2 + y^2 )
     *
     * @param xVal           the x value
     * @param positionRadius the maximum radius of the circle
     * @return y value
     */
    private float getRandomYPosition( float xVal, float positionRadius )
    { //TODO move this into position?
        //get the maximum possible number that the Y value can be, base off of the X value
        float max = (float) Math.sqrt( positionRadius * positionRadius - xVal * xVal );
        return (float) Math.random() * max * 2F - max; //to make sure it can be -ve or +ve
    }
}
