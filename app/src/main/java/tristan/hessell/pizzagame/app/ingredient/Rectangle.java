package tristan.hessell.pizzagame.app.ingredient;

import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * Shape that represents a quadrilateral drawn object.
 * <p/>
 * Created by Tristan on 24/04/2015.
 */
public class Rectangle extends PizzaShape
{
    private final int mProgram;
    private final FloatBuffer vertexBuffer;
    private final float color[];

    public Rectangle( final float length, final float height, final float x, final float y, final float[] color, final float[] rotMat )
    {
        super( rotMat );

        float[] squareCoords = new float[]{
                x - length / 2.0f, y + height / 2.0f, 0.0f,   // top left
                x - length / 2.0f, y - height / 2.0f, 0.0f,   // bottom left
                x + length / 2.0f, y - height / 2.0f, 0.0f,    // bottom right
                x + length / 2.0f, y + height / 2.0f, 0.0f     // top right
        };

        this.color = color;

        // create empty OpenGL ES Program
        mProgram = GLES20.glCreateProgram();

        // add the vertex shader to program
        GLES20.glAttachShader( mProgram, vertexShader );

        // add the fragment shader to program
        GLES20.glAttachShader( mProgram, fragmentShader );

        // creates OpenGL ES program executables
        GLES20.glLinkProgram( mProgram );

        // initialize vertex byte buffer for shape coordinates
        // (# of coordinate values * 4 bytes per float)
        ByteBuffer bb = ByteBuffer.allocateDirect( squareCoords.length * 4 );
        bb.order( ByteOrder.nativeOrder() );
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put( squareCoords );
        vertexBuffer.position( 0 );

        // initialize byte buffer for the draw list
        // (# of coordinate values * 2 bytes per short)
        final short[] drawOrder = {0, 1, 2, 0, 2, 3};
        ByteBuffer dlb = ByteBuffer.allocateDirect( drawOrder.length * 2 );
        dlb.order( ByteOrder.nativeOrder() );
        ShortBuffer drawListBuffer = dlb.asShortBuffer();
        drawListBuffer.put( drawOrder );
        drawListBuffer.position( 0 );
    }

    @Override
    public void draw( final float[] mvpMatrix )
    {
        //add program to  OES environ
        GLES20.glUseProgram( mProgram );

        // get handle to programs vPosition member
        int mPositionHandle = GLES20.glGetAttribLocation( mProgram, "vPosition" );

        //enable handle to the triangles verticies
        GLES20.glEnableVertexAttribArray( mPositionHandle );

        //prepare co-ord data
        final int coOrdsPerVertex = 3;
        //TODO cant remember what stride was for
        final int vertexStride = coOrdsPerVertex * 4;

        GLES20.glVertexAttribPointer( mPositionHandle, coOrdsPerVertex,
                GLES20.GL_FLOAT, false, vertexStride, vertexBuffer );

        // get the fragment shader vColor member
        int mColorHandle = GLES20.glGetUniformLocation( mProgram, "vColor" );

        //set color for drawing the triangle
        GLES20.glUniform4fv( mColorHandle, 1, color, 0 );

        // get handle to shape's transformation matrix
        int mMVPMatrixHandle = GLES20.glGetUniformLocation( mProgram, "uMVPMatrix" );

        // Pass the projection and view transformation to the shader
        GLES20.glUniformMatrix4fv( mMVPMatrixHandle, 1, false, mvpMatrix, 0 );

        //draw triangle
        final int vertexCount = 4;
        GLES20.glDrawArrays( GLES20.GL_TRIANGLE_FAN, 0, vertexCount );
        //disable vertex array
        GLES20.glDisableVertexAttribArray( mPositionHandle );
    }
}
