package tristan.hessell.pizzagame.app.ingredient;

import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Triangle shape used for drawing in OGL20.
 * Always creates an isosceles triangle
 * <p/>
 * Created by Tristan on 24/04/2015.
 */
public class Triangle extends PizzaShape
{
    private final FloatBuffer vertexBuffer;
    private final float color[];
    private final int mProgram;

    /**
     * Constructor for the triangle object
     *
     * @param base     the width of the base of the triangle
     * @param height   the height of the triangle
     * @param x        the x-position of the triangle (from center of the triangle)
     * @param y        the y-position of the triangle (from center of the triangle)
     * @param color    the colour matrix of the shape ( RGBA, 0 - 1 )
     * @param roMatrix the rotation matrix for the shape (16, represents a 4x4 matrix)
     */
    public Triangle( final float base, final float height, final float x, final float y, final float[] color, final float[] roMatrix )
    {
        super( roMatrix );
        float[] triangleCoords = new float[]{
                x + 0.0f, y + height / 2.0F, 0.0f, // top
                x - base / 2.0F, y - height / 2.0F, 0.0f, // bottom left
                x + base / 2.0F, y - height / 2.0F, 0.0f  // bottom right
        };

        this.color = color;

        // initialize vertex byte buffer for shape coordinates (# coordinate values * 4 bytes per float)
        ByteBuffer bb = ByteBuffer.allocateDirect( triangleCoords.length * 4 );
        // use the device hardware's native byte order
        bb.order( ByteOrder.nativeOrder() );

        // create a floating point buffer from the ByteBuffer
        vertexBuffer = bb.asFloatBuffer();
        // add the coordinates to the FloatBuffer
        vertexBuffer.put( triangleCoords );
        // set the buffer to read the first coordinate
        vertexBuffer.position( 0 );

        // create empty OpenGL ES Program
        mProgram = GLES20.glCreateProgram();
        // add the vertex shader to program
        GLES20.glAttachShader( mProgram, vertexShader );
        // add the fragment shader to program
        GLES20.glAttachShader( mProgram, fragmentShader );
        // creates OpenGL ES program executables
        GLES20.glLinkProgram( mProgram );
    }

    /**
     * OGLES calls this method to draw the shape
     *
     * @param mvpMatrix Matrix used by OGLES to draw the shape
     */
    @Override
    public final void draw( final float[] mvpMatrix )
    {
        //add program to  OES environ
        GLES20.glUseProgram( mProgram );

        // get handle to programs vPosition member
        int mPositionHandle = GLES20.glGetAttribLocation( mProgram, "vPosition" );

        //enable handle to the triangles verticies
        GLES20.glEnableVertexAttribArray( mPositionHandle );

        //prepare trianlge coord data
        final int coOrderPerVertex = 3;
        final int vertexStride = coOrderPerVertex * 4;
        GLES20.glVertexAttribPointer( mPositionHandle, coOrderPerVertex, GLES20.GL_FLOAT, false, vertexStride, vertexBuffer );

        // get the fragment shader vColor member
        final int mColorHandle = GLES20.glGetUniformLocation( mProgram, "vColor" );
        //set color for drawing the triangle
        GLES20.glUniform4fv( mColorHandle, 1, color, 0 );

        // get handle to shape's transformation matrix
        final int mMVPMatrixHandle = GLES20.glGetUniformLocation( mProgram, "uMVPMatrix" );

        // Pass the projection and view transformation to the shader
        GLES20.glUniformMatrix4fv( mMVPMatrixHandle, 1, false, mvpMatrix, 0 );

        //draw triangle
        final int vertexCount = 3;
        GLES20.glDrawArrays( GLES20.GL_TRIANGLES, 0, vertexCount );
        //disable vertex array
        GLES20.glDisableVertexAttribArray( mPositionHandle );
    }
}
