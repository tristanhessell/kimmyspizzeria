package tristan.hessell.pizzagame.app;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Score class is used to represent a scores that previous players have.
 *
 * Used to represent the name - score relationship
 *
 * Created by Tristan on 24/05/2015.
 */
public final class Score implements Comparable<Score>, Parcelable
{
    private final int score;
    private final String player;

    public static final Parcelable.Creator<Score> CREATOR = new Parcelable.Creator<Score>()
    {
        public final Score createFromParcel(final Parcel in) {
            return new Score(in);
        }

        public final Score[] newArray(final int size) {
            return new Score[size];
        }
    };

    /**
     * Constructor.
     *
     * @param player The players name
     * @param score  The players score
     */
    public Score( final String player, final int score )
    {
        this.score = score;
        this.player = player;
    }

    public Score(final Parcel in)
    {
        score = in.readInt();
        player = in.readString();
    }

    /**
     * Gets the player score
     *
     * @return The score
     */
    public final int getScore()
    {
        return score;
    }

    /**
     * Gets the player name
     *
     * @return The player name
     */
    public final String getPlayer()
    {
        return player;
    }

    /**
     * Returns an integer based on the parameter Score relative to this score
     * -1 : this score ranks higher than the parameter
     *  0 : the scores are equal
     *  1 : this score ranks lower than the parameter
     *
     * @param anotherScore the value of the other score
     * @return 0 if this score is equal, 1 if param is less than this score, -1 if param is greater than this score.
     */
    public final int compareScore( final int anotherScore )
    {
        if( this.score > anotherScore )
        {
            return -1;
        }
        else if( this.score < anotherScore )
        {
            return 1;
        }

        return 0; //otherwise they are equal
    }


    @Override
    /**
     * Returns an integer based on the parameter Score relative to this score
     * -1 : this score ranks higher than the parameter
     *  0 : the scores are equal
     *  1 : this score ranks lower than the parameter
     *
     *  @param another The score object to compare to
     */
    public final int compareTo( final Score another )
    {
        return compareScore( another.score );
    }

    @Override
    public final String toString()
    {
        return player + ": " + score;
    }

    @Override
    public final int describeContents()
    {
        return 0;
    }

    @Override
    public final void writeToParcel( final Parcel dest, final int flags )
    {
        dest.writeInt( score );
        dest.writeString( player );
    }

    @Override
    public final boolean equals(final Object obj)
    {
        if(obj instanceof Score)
        {
            Score inScore = (Score)obj;
            return this.score == inScore.score && this.player.equals( inScore.player );
        }
        return false;
    }
}
