package tristan.hessell.pizzagame.app;

import android.animation.AnimatorInflater;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import tristan.hessell.pizzagame.app.ingredient.INGREDIENT;
import tristan.hessell.pizzagame.app.ingredient.IngredientButton;
import tristan.hessell.pizzagame.app.pizza.PizzaFactory;
import tristan.hessell.pizzagame.app.pizza.PizzaSurfaceView;

import java.util.ArrayList;
import java.util.List;

/**
 * Main game activity for the Pizza game.
 * <p/>
 * Won't be the activity that the user sees of the bat, there will be a landing page.
 */
public class MainActivity extends Activity
{
    public static final int ORDER_FULFILLED = 1;
    private static final int MAX_INGREDIENTS = 13;
    private static final int ROUND_DURATION = 10;
    private ArrayList<INGREDIENT> currentIngredients = new ArrayList<>(); //needs to be an AL due to being parcel'd

    private TextView tvTime;
    private TextView tvScore;
    private PizzaSurfaceView mGLView;
    private OrderedPizzaView order1;
    private OrderedPizzaView order2;
    private OrderedPizzaView order3;
    private Toast toast;
    private int score = 0;
    private int seconds;

    private LinearLayout orderLane;

    private final Runnable gameTimerRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            seconds--;
            tvTime.setText( String.format( "%1$2d", seconds ) );

            if( seconds <= 0 )
            {
                //end the game here
                handler.removeCallbacksAndMessages( null );

                //show the scoreboard activity.
                Intent intent = new Intent( MainActivity.this, ScoreboardActivity.class );
                intent.putExtra( "score", score );
                startActivity( intent );
                finish();
            } else
            {
                //update the value of the textview
                handler.postDelayed( this, 1000 );
            }
        }
    };

    private final Handler handler = new Handler()
    {
        @Override
        public void handleMessage( Message msg )
        {
            switch( msg.what )
            {
                case ORDER_FULFILLED: //scored it
                    OrderedPizzaView opv = (OrderedPizzaView) msg.obj;
                    if( opv.ingredientsMatch( currentIngredients ) )
                    {
                        clearIngredients();
                        score += msg.arg1;
                        tvScore.setText( String.format( "%1$2d", score ) );

                        //remove & adding back in are so the order is animated into the list
                        orderLane.removeView( opv );
                        orderLane.addView( new OrderedPizzaView( MainActivity.this, PizzaFactory.getRandomPizza(), this )
                                , new LinearLayout.LayoutParams( ViewGroup.LayoutParams.MATCH_PARENT, 0, 1 ) );

                    } else
                    {
                        ObjectAnimator orderBkgAnim = (ObjectAnimator) AnimatorInflater.loadAnimator( MainActivity.this, R.animator.animate_bkgd_col );
                        orderBkgAnim.setEvaluator( new ArgbEvaluator() );
                        orderBkgAnim.setTarget( opv );
                        orderBkgAnim.start();
                    }
                    break;
            }
        }
    };

    @Override
    public void onBackPressed()
    {
        showPauseMenu();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        showPauseMenu();
        mGLView.onPause();
    }

    @Override
    protected void onSaveInstanceState( Bundle outState )
    {
        outState.putParcelableArrayList( "ingredients", currentIngredients );
        outState.putInt( "score", score );
        outState.putInt( "timeLeft", seconds );

        ArrayList<String> strOrders = new ArrayList<>( 3 );
        strOrders.add( order1.getPizzaName() );
        strOrders.add( order2.getPizzaName() );
        strOrders.add( order3.getPizzaName() );
        outState.putStringArrayList( "orders", strOrders );
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        handler.removeCallbacksAndMessages( null );
        //to avoid the memory leak potential problem from the handler
        // see: https://techblog.badoo.com/blog/2014/08/28/android-handler-memory-leaks/
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        mGLView.onResume();
    }

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        tvScore   = (TextView)         findViewById( R.id.tvscore );
        mGLView   = (PizzaSurfaceView) findViewById( R.id.surfaceview );
        tvTime    = (TextView)         findViewById( R.id.tvtime );
        order1    = (OrderedPizzaView) findViewById( R.id.order1 );
        order2    = (OrderedPizzaView) findViewById( R.id.order2 );
        order3    = (OrderedPizzaView) findViewById( R.id.order3 );
        orderLane = ((LinearLayout)    findViewById( R.id.orderLane ));

        toast = null;

        //if this is is the first time the activity is being created
        if( savedInstanceState == null )
        {
            seconds = ROUND_DURATION;
            order1.initialise( PizzaFactory.getRandomPizza(), handler );
            order2.initialise( PizzaFactory.getRandomPizza(), handler );
            order3.initialise( PizzaFactory.getRandomPizza(), handler );
            handler.postDelayed( gameTimerRunnable, 1000 );
        } else
        { //else the activity has been paused for what ever reason
            score = savedInstanceState.getInt( "score" );
            seconds = savedInstanceState.getInt( "timeLeft" );
            currentIngredients = savedInstanceState.getParcelableArrayList( "ingredients" ) ;
            mGLView.addIngredients( currentIngredients );

            final List<String> strOrders = savedInstanceState.getStringArrayList( "orders" );
            order1.initialise( PizzaFactory.getPizza( strOrders.get( 0 ) ), handler );
            order2.initialise( PizzaFactory.getPizza( strOrders.get( 1 ) ), handler );
            order3.initialise( PizzaFactory.getPizza( strOrders.get( 2 ) ), handler );
            onPause();
        }
        tvTime.setText( String.format( "%1$2d", seconds ) );
    }

    /**
     * Adds an ingredient to the pizza, unless there are too many ingredients (to which it warns the user)
     *
     * @param v Required for the XML onClick assignment
     */
    public void onIngredientButtonClick( View v )
    {
        //if there there is enough ingredients
        if( currentIngredients.size() >= MAX_INGREDIENTS )
        {
            //and if the toast is not showing already
            if( toast == null || toast.getView() == null || !toast.getView().isShown() )
            {
                //show the toast - this stops the toast hanging around for ages if they spam buttons
                toast = Toast.makeText( this, "You can only have " + MAX_INGREDIENTS + " ingredients on the pizza!", Toast.LENGTH_SHORT );
                toast.show();
            }
        } else
        { //add the ingredients to the surface view
            final INGREDIENT ing = ((IngredientButton) v).getIngredient();
            mGLView.addIngredient( ing );
            currentIngredients.add( ing );
        }
    }

    /**
     * Clears the ingredients from the pizza
     * @param v Required for the XML onClick assignment
     */
    public void btnClearIngredientsClick( View v )
    {
        clearIngredients();
    }

    /**
     * Clears the ingredients from the pizza
     */
    private void clearIngredients()
    {
        currentIngredients.clear();
        mGLView.clearPizza();
    }

    /**
     * Shows the pause menu
     *
     * @param v Required for the XML onClick assignment
     */
    public void btnPauseClick( View v )
    {
        showPauseMenu();
    }

    /**
     * Pauses the game and shows the pause menu if it isn't shown already.
     */
    private void showPauseMenu()
    {
        //stop the timer
        //this will mean that the person's time will reset to the second,
        //but I'm okay for that to happen
        handler.removeCallbacksAndMessages( null );

        //if the pause dialog is not shown yet, show it
        if( getFragmentManager().findFragmentByTag( "Diag" ) == null )
        {
            PauseFragmentDialog pauseDlg = new PauseFragmentDialog();
            pauseDlg.show( getFragmentManager(), "Diag" );
        }
    }

    /**
     * StartTimer is called via the PauseDialogFragment when the pause menu is closed.
     */
    public void startTimer()
    {
        handler.postDelayed( gameTimerRunnable, 1000 );
    }
}
