package tristan.hessell.pizzagame.app;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;

public class ScoreDialogFragment extends DialogFragment
{
    private static final String TITLE_IDENTIFIER    = "title";
    private static final String CALLBACK_IDENTIFIER = "scores";
    private static final String ITEMS_IDENTIFIER    = "listItems";

    private String title;
    private ArrayList<Score> scores;

    public static ScoreDialogFragment newInstance(String inTitle, ArrayList<Score> scores)
    {
        ScoreDialogFragment frag = new ScoreDialogFragment();
        Bundle args = new Bundle();
        args.putString( TITLE_IDENTIFIER, inTitle );
        args.putParcelableArrayList( CALLBACK_IDENTIFIER, scores );
        frag.setArguments( args );

        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        getValuesFromArguments( getArguments() );

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate( R.layout.dialog_score_list, null );

        //get the list view, load it with the items.
        ScoreListAdapter adapter = new ScoreListAdapter( getActivity(), scores );

        ListView listView = (ListView)v.findViewById( R.id.listview );
        listView.setAdapter( adapter );
        listView.setItemsCanFocus( false );

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle( title )
            .setView( v )
            .setPositiveButton( "OK",
                new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick( DialogInterface dialog, int whichButton )
                    {
                        //TODO figure out clicks
                    }
                } )
            .setNegativeButton( "Cancel",
                new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick( DialogInterface dialog, int whichButton )
                    {
                        //TODO figure out clicks

                    }
                } );
        return builder.create();
    }

    //if a value isnt present, a default is used
    //Title, callback and the list of strings are required - exception will be thrown if they are not present
    private void getValuesFromArguments(Bundle args)
    {
        if(args == null)
        {
            throw new IllegalArgumentException( "Cannot create Dialog: No arguments passed to dialog" );
        }

        title = args.getString( TITLE_IDENTIFIER );
        if(title == null)
        {
            throw new IllegalArgumentException( "Cannot create Dialog: No Title passed via arguments" );
        }

        scores = args.getParcelableArrayList( ITEMS_IDENTIFIER );
        if( scores == null)
        {
            throw new IllegalArgumentException( "Cannot create Dialog: No list entries passed via arguments" );
        }
    }

    public ScoreDialogFragment setListItems(ArrayList<Score> list)
    {
        getArguments().putParcelableArrayList( ITEMS_IDENTIFIER, list );
        return this;
    }
}
