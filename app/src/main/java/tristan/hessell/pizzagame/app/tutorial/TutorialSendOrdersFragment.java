package tristan.hessell.pizzagame.app.tutorial;

import android.animation.AnimatorInflater;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import tristan.hessell.pizzagame.app.OrderedPizzaView;
import tristan.hessell.pizzagame.app.R;
import tristan.hessell.pizzagame.app.ingredient.INGREDIENT;
import tristan.hessell.pizzagame.app.pizza.Pizza;
import tristan.hessell.pizzagame.app.pizza.PizzaFactory;
import tristan.hessell.pizzagame.app.pizza.PizzaSurfaceView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Tutorial that shows what happens when you click on a order thats correct/incorrect
 */
public class TutorialSendOrdersFragment extends ATutorialFragment
{
    private static final String INGREDIENTS_CODE = "ingredients";
    private static final String ORDERS_CODE = "orders";
    private ArrayList<String> pizzas;
    private LinearLayout orderLane;
    private ArrayList<INGREDIENT> ingredients;
    private LinearLayout.LayoutParams lp;
    private PizzaSurfaceView pizzaSurfaceView;
    /* Handler is used to take the order clicks and send/reject the order */
    private final Handler handler = new Handler()
    {
        @Override
        public void handleMessage( Message msg )
        {
            OrderedPizzaView opv = (OrderedPizzaView) msg.obj;
            if( opv.ingredientsMatch( ingredients ) )
            {
                //remove the pizza from the list
                pizzas.remove( opv.getPizzaName() );
                pizzaSurfaceView.clearPizza();
                orderLane.removeView( opv );

                Pizza pizz = PizzaFactory.getRandomPizza();
                pizzas.add( pizz.getName() );
                ingredients = getPizzaIngredients();

                pizzaSurfaceView.addIngredients( ingredients );
                orderLane.addView( new OrderedPizzaView( getActivity(), pizz, this ), lp );
            } else
            {
                ObjectAnimator orderBkgAnim = (ObjectAnimator) AnimatorInflater.loadAnimator( getActivity(), R.animator.animate_bkgd_col );
                orderBkgAnim.setEvaluator( new ArgbEvaluator() );
                orderBkgAnim.setTarget( opv );
                orderBkgAnim.start();
            }
        }
    };

    public TutorialSendOrdersFragment()
    {}

    @Override
    public String getName()
    {
        return "Sending Orders";
    }

    @Override
    public void onSaveInstanceState( Bundle outState )
    {
        super.onSaveInstanceState( outState );
        outState.putParcelableArrayList( INGREDIENTS_CODE, ingredients );
        outState.putStringArrayList( ORDERS_CODE, pizzas );
    }


    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState )
    {
        View v = inflater.inflate( R.layout.fragment_sending_orders_tutorial, container, false );

        ((TextView) v.findViewById( R.id.tvContent )).setText( Html.fromHtml( getString( R.string.tutorial_send_order ) ) );
        orderLane = ((LinearLayout) v.findViewById( R.id.orderLane ));
        pizzaSurfaceView = ((PizzaSurfaceView) v.findViewById( R.id.surfaceview ));
        //Not entirely happy I did this, but it works (used for sizing the pizza views)
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize( size );
        lp = new LinearLayout.LayoutParams( ViewGroup.LayoutParams.MATCH_PARENT, size.y / 5 );

        if( savedInstanceState == null )
        {
            pizzas = new ArrayList<>();
            Pizza p1 = PizzaFactory.getRandomPizza();
            Pizza p2 = PizzaFactory.getRandomPizza();
            Pizza p3 = PizzaFactory.getRandomPizza();

            pizzas.add( p1.getName() );
            pizzas.add( p2.getName() );
            pizzas.add( p3.getName() );

            //Create and add the orderviews to the tutorial - need to do this programatically
            //to get the order view just right
            orderLane.addView( new OrderedPizzaView( getActivity(), p1, handler ), lp );
            orderLane.addView( new OrderedPizzaView( getActivity(), p2, handler ), lp );
            orderLane.addView( new OrderedPizzaView( getActivity(), p3, handler ), lp );

            //get the surface view and add ingredients to it

            ingredients = getPizzaIngredients();

        } else
        {
            //Create and add the orderviews to the tutorial - need to do this programatically
            //to get the order view just right
            pizzas = savedInstanceState.getStringArrayList( ORDERS_CODE );
            orderLane.addView( new OrderedPizzaView( getActivity(), PizzaFactory.getPizza( pizzas.get( 0 ) ), handler ), lp );
            orderLane.addView( new OrderedPizzaView( getActivity(), PizzaFactory.getPizza( pizzas.get( 1 ) ), handler ), lp );
            orderLane.addView( new OrderedPizzaView( getActivity(), PizzaFactory.getPizza( pizzas.get( 2 ) ), handler ), lp );

            //get the surface view and add ingredients to it
            ingredients = savedInstanceState.getParcelableArrayList( INGREDIENTS_CODE );

        }

        pizzaSurfaceView.addIngredients( ingredients );

        return v;
    }

    private ArrayList<INGREDIENT> getPizzaIngredients()
    {
        //get a random pizza
        Pizza pizz = PizzaFactory.getPizza( pizzas.get( (int) (Math.random() * 3) ) );
        ArrayList<INGREDIENT> ingreds = new ArrayList<>( pizz.getIngredients() );
        Collections.sort( ingreds, new Comparator<INGREDIENT>()
        {
            @Override
            public int compare( INGREDIENT lhs, INGREDIENT rhs )
            {
                int num = 0;
                if( lhs.name.equals( "Sauce" ) || rhs.name.equals( "Cheese" ) )
                {
                    num = -1;
                }
                if( lhs.name.equals( "Cheese" ) || rhs.name.equals( "Sauce" ) )
                {
                    num = 1;
                }

                return num;
            }
        } );

        return ingreds;
    }
}
