package tristan.hessell.pizzagame.app.ingredient;

import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Class responsible for a circle shape to be drawn using OGL20.
 * <p/>
 * Created by Tristan on 24/04/2015.
 */
public class Circle extends PizzaShape
{
    private final int mProgram;
    private final FloatBuffer mVertexBuffer;
    private final float color[];

    public Circle( final float radius, final float x, final float y, final float[] inColor, final float[] inRot )
    {
        super( inRot );
        color = inColor;

        final float vertices[] = new float[364 * 3];

        for( int i = 0; i < 364; i++ )
        {
            vertices[(i * 3)] = (x + (float) (radius * Math.cos( (3.14 / 180) * (float) i )));
            vertices[(i * 3) + 1] = (y + (float) (radius * Math.sin( (3.14 / 180) * (float) i )));
            vertices[(i * 3) + 2] = 0;
        }

        final ByteBuffer vertexByteBuffer = ByteBuffer.allocateDirect( vertices.length * 4 );
        vertexByteBuffer.order( ByteOrder.nativeOrder() );
        mVertexBuffer = vertexByteBuffer.asFloatBuffer();
        mVertexBuffer.put( vertices );
        mVertexBuffer.position( 0 );

        mProgram = GLES20.glCreateProgram();             // create empty OpenGL ES Program
        GLES20.glAttachShader( mProgram, vertexShader );   // add the vertex shader to program
        GLES20.glAttachShader( mProgram, fragmentShader ); // add the fragment shader to program
        GLES20.glLinkProgram( mProgram );
    }

    @Override
    public final void draw( final float[] mvpMatrix )
    {
        GLES20.glUseProgram( mProgram );

        // get handle to vertex shader's vPosition member
        int mPositionHandle = GLES20.glGetAttribLocation( mProgram, "vPosition" );

        // Enable a handle to the triangle vertices
        GLES20.glEnableVertexAttribArray( mPositionHandle );

        // Prepare the triangle coordinate data
        GLES20.glVertexAttribPointer( mPositionHandle, 3,
                GLES20.GL_FLOAT, false, 12
                , mVertexBuffer );

        // get handle to fragment shader's vColor member
        int mColorHandle = GLES20.glGetUniformLocation( mProgram, "vColor" );

        // Set color for drawing the triangle
        GLES20.glUniform4fv( mColorHandle, 1, color, 0 );

        int mMVPMatrixHandle = GLES20.glGetUniformLocation( mProgram, "uMVPMatrix" );

        // Apply the projection and view transformation
        GLES20.glUniformMatrix4fv( mMVPMatrixHandle, 1, false, mvpMatrix, 0 );

        // Draw the triangle
        GLES20.glDrawArrays( GLES20.GL_TRIANGLE_FAN, 0, 364 );

        // Disable vertex array
        GLES20.glDisableVertexAttribArray( mPositionHandle );
    }
}
