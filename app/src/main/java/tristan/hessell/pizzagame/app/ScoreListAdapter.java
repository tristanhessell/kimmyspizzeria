package tristan.hessell.pizzagame.app;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;
import tristan.hessell.pizzagame.app.ingredient.INGREDIENT;
import tristan.hessell.pizzagame.app.ingredient.IngredientView;

import java.util.List;

/**
 * Adapter for the Ingredients. This allows the list of IngredientView to be used with a GridView.
 * <p/>
 * Created by Tristan on 20/05/2015.
 */
public class ScoreListAdapter extends ArrayAdapter
{
    private static final int COLOUR_GOLD   = Color.parseColor("#FFD700");
    private static final int COLOUR_SILVER = Color.parseColor("#C0C0C0");
    private static final int COLOUR_BRONZE = Color.parseColor("#CD7F32");
    private static final int COLOUR_HIGHLIGHTED = Color.RED;
    private static final int LAYOUT_RESOURCE_ID = android.R.layout.simple_list_item_1;

    private final Context context;
    private final List<Score> scores;

    private View inflatedView;
    private ViewGroup parentList;
    public ScoreListAdapter( final Context c, final List<Score> inScores)
    {
        super(c,  LAYOUT_RESOURCE_ID);
        context = c;
        scores = inScores;
    }

    @Override
    public int getCount()
    {
        return scores.size();
    }

    @Override
    public Object getItem( final int position )
    {
        return scores.get( position );
    }

    @Override
    public long getItemId( final int position )
    {
        return position;
    }

    @Override
    public View getView( final int position, final View convertView, final ViewGroup parent )
    {
        final TextView textView;
        if(parentList == null)
        {
            parentList = parent;
        }
        //This block is required for a GridView, where there is only a small number are actually created,
        //instead the contents of the view are updates when required.
        if( convertView == null )
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            inflatedView= inflater.inflate(LAYOUT_RESOURCE_ID, parent,false);
            textView = ((TextView)inflatedView.findViewById( android.R.id.text1 ) );
        } else
        {
            textView = (TextView) convertView;
        }

        textView.setText( getPositionString( position+1 ) + " " + scores.get( position ).toString() );

        if( position == 0)
        {
            textView.setTextColor( COLOUR_GOLD );
        }
        else if(position == 1)
        {
            textView.setTextColor( COLOUR_SILVER );
        }
        else if(position == 2)
        {
            textView.setTextColor( COLOUR_BRONZE );
        }
        else
        {
            textView.setTextColor( Color.BLACK );
        }

        return textView;
    }

    public final void highlightRow(int rowIndex)
    {
        if( rowIndex >= scores.size() )
        {
            throw new IllegalArgumentException( "Cannot highlight row outside of range" );
        }

        //TODO
    }

    /**
     * Gets the string representation (1st, 2nd etc) of the position specified.
     * No error checking in this case.
     *
     * @param pos number between 1 - 10
     * @return A string representation of the number
     */
    private String getPositionString( final int pos )
    {
        if( pos == 1 )
        {
            return "1st";
        } else if( pos == 2 )
        {
            return "2nd";
        } else if( pos == 3 )
        {
            return "3rd";
        } else
        {
            return pos + "th";
        }
    }
}
