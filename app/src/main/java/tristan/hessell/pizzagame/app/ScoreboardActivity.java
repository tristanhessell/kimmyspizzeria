package tristan.hessell.pizzagame.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Activity that contains the scoreboard after a user has finished their runthrough of the game
 */
public final class ScoreboardActivity extends Activity
{
    private static final int MAX_LEADERBOARD_LENGTH = 10;
    private final String SCORES_FILENAME = "scores.xml";

    private ArrayList<Score> scores = new ArrayList<>();
    private boolean scoredAlready = false;
    private ScoreListAdapter adapter;

    @Override
    protected void onCreate( final Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_scoreboard );

        //if the game hasn't been paused or something
        if(savedInstanceState != null)
        {
            scoredAlready = savedInstanceState.getBoolean( "scoredAlready" );
            scores =  savedInstanceState.getParcelableArrayList( "scores" ) ;
        }
        else
        {
            loadScoresFromFile( scores );
        }

        final int score = getScore( getIntent() );

        //if the score is on the top 10, ask for the players name
        if( !scoredAlready && scoreGetsOnBoard(score, scores) )
        {
            NameEntryDialogFragment.newInstance( "Congratulations!", new DialogCallback()
            {
                @Override
                public void onCallback( String name )
                {
                    final Score newScore = new Score( name, score );
                    scores.add( newScore );
                    Collections.sort( scores );

                    if( scores.size() > MAX_LEADERBOARD_LENGTH )
                    {//if there are too many scores, remove the last one from the list
                        scores.remove( scores.size() - 1 );
                    }

                    adapter.notifyDataSetChanged();
                    //if the score was added - it wont be removed

                    saveScoresToFile(scores);
                    scoredAlready = true;
                }
            } ).setMessage( "You completed " + score + " pizzas - You are on the leader board!" )
                    .show( getFragmentManager(), "dlg" );
        }
        else
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle( "Congratulations!" )
                    .setMessage( "You completed " + score + " pizzas!" )
                    .setPositiveButton( "OK", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick( DialogInterface dialog, int which )
                        {
                            //
                        }
                    } );
            builder.create().show();
        }

        adapter = new ScoreListAdapter( this, scores );
        ((ListView) findViewById( R.id.scorelist )).setAdapter( adapter );
    }

    @Override
    protected void onSaveInstanceState( final Bundle outState )
    {
        outState.putBoolean( "scoredAlready", scoredAlready );
        outState.putParcelableArrayList( "scores", scores );
    }

    /**
     * Gets the users score from the intent.
     *
     * @return Score of the user [0, infinity )
     * @throws IllegalArgumentException if score cannot be found
     */
    private int getScore(final Intent intent)
    {
        int scoreNum = intent.getIntExtra( "score", -1 );

        if( scoreNum == -1 )
        {
            throw new IllegalArgumentException( "Not able to get score from bundle " );
        }

        return scoreNum;
    }

    /**
     * Called when the user want to play again
     *
     * @param v Required for the XML onClick assignment
     */
    public void btnPlayAgain( final View v )
    {
        //start the game again
        startActivity( new Intent( this, MainActivity.class ) );
        finish();
    }

    /**
     *
     * @param v
     */
    public void btnResetScoresClick(View v)
    {
        //ask the user if they are sure
        new AlertDialog.Builder(this)
            .setTitle( "Are you sure?" )
            .setMessage( "Resetting the scoreboard cannot be undone" )
            .setPositiveButton( "OK", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick( DialogInterface dialog, int which )
                {
                    try
                    {
                        //clear the scores from the list
                        scores.clear();
                        adapter.notifyDataSetChanged();

                        //overwrites the scores file
                        openFileOutput( SCORES_FILENAME, Context.MODE_PRIVATE );
                    }
                    catch( Exception ex )
                    {
                        System.out.println( "cannot clear scores" );
                    }
                }
            } )
            .setNegativeButton( "Cancel", new DialogInterface. OnClickListener()
            {
                @Override
                public void onClick( DialogInterface dialog, int which )
                {
                    //just close the dialog
                }
            } )
            .create()
            .show();
    }

    /**
     *  Gets the position of the current score
     * if your score is lower, keep goings through the list (until the end is reached)
     *
     * @param score
     * @param scores
     * @return the position of the current score on the scoreboard [ 0, 10 ]
     */
    private boolean scoreGetsOnBoard(final int score, final List<Score> scores)
    {
        if( scores.size() < MAX_LEADERBOARD_LENGTH )
        {
            return true;
        }

        //if the first score equals or comes after, then true
        if( scores.get( 0 ).compareScore( score ) >= 0)
        {
            return true;
        }

        //if the score comes after the last or is equal last, return false
        return !(scores.get( scores.size() - 1 ).compareScore( score ) <= 0);
    }



    /**
     * Saves the score in the scores list to file.
     */
    private void saveScoresToFile(final List<Score> scores)
    {
        try
        {
            FileOutputStream outStream = openFileOutput( SCORES_FILENAME, Context.MODE_PRIVATE );
            StringBuilder strBuild = new StringBuilder( "<Scores>" );

            for( Score sc : scores )
            {
                strBuild.append( "<Score><Name>" )
                        .append( sc.getPlayer() )
                        .append( "</Name><Value>" )
                        .append( sc.getScore() )
                        .append( "</Value></Score>" );
            }

            strBuild.append( "</Scores>" );
            outStream.write( strBuild.toString().getBytes() );
            outStream.close();
        }
        catch( IOException e )
        {
            System.out.println( "cant save the file for scores" );
        }
    }

    /**
     * Loads the scores from the file into the score list.
     *
     * @param scores The List to be loaded with scores
     */
    private void loadScoresFromFile( final List<Score> scores )
    {
        try
        {
            List<Element> scoresXML = new SAXBuilder().build( openFileInput( SCORES_FILENAME ) )
                    .getRootElement().getChildren( "Score" );

            if( scoresXML.size() > MAX_LEADERBOARD_LENGTH )
            {
                throw new IllegalArgumentException( "Too many scores in scores XML file" );
            }

            for( Element scoreXML : scoresXML )
            {
                scores.add( new Score( scoreXML.getChildText( "Name" ), Integer.parseInt( scoreXML.getChildText( "Value" ) ) ) );
            }
            Collections.sort( scores );
        }
        catch( IOException | JDOMException e )
        {
            System.out.println( "cant file the file for scores" );
        }
    }
}
