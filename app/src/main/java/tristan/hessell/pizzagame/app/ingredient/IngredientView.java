package tristan.hessell.pizzagame.app.ingredient;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import tristan.hessell.pizzagame.app.R;

/**
 * Representation of an ingredient - used in the ingredients page (not the actual game - that uses IngredientButtons)
 * Doesn't actually need to store what the ingredient is - yet.
 * Created by Tristan on 20/05/2015.
 */
public class IngredientView extends LinearLayout
{
    //private INGREDIENT ingredient;
    private ImageView imIngredient;
    private TextView tvName;

    /**
     * XML constructor.
     * Requires app:ingredient="" to work properly
     *
     * @param context The parent that houses the view
     * @param attrs   attribute set passed where the xml attributes are taken from
     */
    public IngredientView( final Context context, final AttributeSet attrs )
    {
        super( context, attrs );
        ((LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE ))
                .inflate( R.layout.ingredientview, this, true );

        TypedArray a = context.obtainStyledAttributes( attrs,
                R.styleable.IngredientView, 0, 0 );

        initialiseView();
        setIngredient( INGREDIENT.valueOf( a.getString( R.styleable.IngredientView_ingredient ).toUpperCase() ) );

        a.recycle();
    }

    /**
     * Programatic constructor.
     * INGREDIENT parameter is required to determine what ingredient this view represents
     *
     * @param context The parent that houses this view
     * @param ing The ingredient that this view represents
     */
    public IngredientView( final Context context, final INGREDIENT ing )
    {
        super( context );
        ((LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE ))
                .inflate( R.layout.ingredientview, this, true );

        initialiseView();
        setIngredient( ing );
    }

    /**
     * Sets the view components as required on construction.
     */
    private void initialiseView()
    {
        imIngredient = (ImageView) findViewById( R.id.imIngredient );
        tvName = (TextView) findViewById( R.id.tvName );
    }

    /**
     * Changes the ingredient that this IngredientView represents.
     *
     * @param ing The ingredient that you want to change this view to represent.
     */
    public final void setIngredient( final INGREDIENT ing )
    {
        //ingredient = ing;
        imIngredient.setImageResource( ing.resId );
        tvName.setText( ing.name);
    }
}
