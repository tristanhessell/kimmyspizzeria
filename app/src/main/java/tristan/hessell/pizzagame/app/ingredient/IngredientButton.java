package tristan.hessell.pizzagame.app.ingredient;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageButton;
import tristan.hessell.pizzagame.app.R;

/**
 * Ingredientbutton is used to add ingredients to the pizza, when
 * the user clicks on the button.
 *
 * Contains the image of the ingredient and a red border
 *
 * Created by Tristan on 23/04/2015.
 */
public class IngredientButton extends ImageButton
{
    private final INGREDIENT ingredient;

    /**
     * Constructor used when the button is generated programatically.
     * Haven't had to do that just yet....
     *
     * @param context      the context the view is running on
     * @param inIngredient the INGREDIENT that this button will add to the pizza
     * @see INGREDIENT
     */
    public IngredientButton( final Context context, final INGREDIENT inIngredient )
    {
        super( context );

        ingredient = inIngredient;
        initialiseButton();
    }

    /**
     * Constructor for when the button is declared in an xml file
     *
     * @param context the context the view is running on
     * @param attrs passed into constructor when class is used in xml
     */
    public IngredientButton( final Context context, final AttributeSet attrs )
    {
        super( context, attrs );

        TypedArray a = context.obtainStyledAttributes( attrs,
                R.styleable.IngredientButton, 0, 0 );
        ingredient = INGREDIENT.valueOf( a.getString( R.styleable.IngredientButton_ingredient ).toUpperCase() );
        a.recycle();
        initialiseButton();
    }

    /**
     * Gets the ingredient that this button represents.
     *
     * @return ingredient enum stored in this class
     * @see INGREDIENT
     */
    public INGREDIENT getIngredient()
    {
        return ingredient;
    }

    /**
     * Sets the little UI settings for the button (including the image of the ingredient)
     */
    private void initialiseButton()
    {
        this.setPadding( 20, 20, 20, 20 );
        this.setBackgroundResource( R.drawable.border );
        this.setImageResource( ingredient.resId );
        this.setAdjustViewBounds( true );
    }
}
