package tristan.hessell.pizzagame.app.pizza;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import tristan.hessell.pizzagame.app.ingredient.*;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Controls what is drawn on the associated GLSurfaceView.
 *
 * Generally, you need to apply both the projection transformation and camera transformation
 *
 * GL10 parameters are reused in the 2.0 API to make the Android code simpler.
 * Created by Tristan on 24/04/2015.
 */
public class PizzaRenderer implements GLSurfaceView.Renderer
{
    private Circle pizzaBase;
    private final List<InfoClass> infoList = Collections.synchronizedList( new ArrayList<InfoClass>() );
    private final List<PizzaShape> shapeList = Collections.synchronizedList( new ArrayList<PizzaShape>() );
    // mMVPMatrix is an abbreviation for "Model View Projection Matrix"
    private final float[] mMVPMatrix = new float[16];
    private final float[] mProjectionMatrix = new float[16];
    private final float[] mViewMatrix = new float[16];

    private final float baseRadius = 0.9F;
    private final float sauceRadius = baseRadius - .15F;
    private final float positionRadius = sauceRadius - 0.05F;

    /***
     * Called once to set up the views OpenGL ES environment.
     *
     * @param gl Unused in OGL20.
     * @param config not sure!
     */
    @Override
    public void onSurfaceCreated( GL10 gl, EGLConfig config )
    {
        //GLES20.glClearColor( 1.0f, 0.0f, 0.0f, 0f );
        GLES20.glLineWidth( 20F );
        drawPizzaBase();
        //put them here if the coordinates dont change (for processing efficiency).
    }

    /**
     * Called when the device geometry changes (orientation etc).
     * @param gl Unused in OGL20.
     * @param width New width of the viewport.
     * @param height New height of the viewport.
     */
    @Override
    public void onSurfaceChanged( GL10 gl, int width, int height )
    {
        GLES20.glViewport( 0, 0, width, height );

        float ratio = (float) width / height;

        // this projection matrix is applied to object coordinates
        // in the onDrawFrame() method
        Matrix.frustumM( mProjectionMatrix, 0, -ratio, ratio, -1, 1, 3, 7 );
    }

    /**
     * Called for each redraw of the view.
     * @param gl Unused in GL20.
     */
    @Override
    public void onDrawFrame( GL10 gl )
    {
        GLES20.glClear( GLES20.GL_COLOR_BUFFER_BIT );

        // Set the camera position (View matrix)
        Matrix.setLookAtM( mViewMatrix, 0, 0, 0, -3, 0f, 0f, 0f, 0f, 1.0f, 0.0f );

        // Calculate the projection and view transformation
        Matrix.multiplyMM( mMVPMatrix, 0, mProjectionMatrix, 0, mViewMatrix, 0 );

        // Draw the base of the pizza
        pizzaBase.draw( mMVPMatrix );

        //get the shapes that need to be drawn
        synchronized( infoList )
        {
            synchronized( shapeList )
            {
                for( InfoClass info : infoList )
                {
                    shapeList.add( ShapeCreator.getShape( info ) );
                }
            }
            infoList.clear();
        }

        //draw all the shapes in the shape list
        synchronized( shapeList )
        {
            for( PizzaShape shape : shapeList )
            {
                //set the rotation matrix
                //PizzaShape shape = ShapeCreator.getShape( info );
                float[] scratch = new float[16];

                Matrix.multiplyMM( scratch, 0, mMVPMatrix, 0, shape.getRotationMatrix(), 0);
                shape.draw( scratch );
            }
        }
    }

    /**
     * Load shader is used when drawing a shape.
     *
     * @param type       not sure!
     * @param shaderCode not sure!
     * @return not sure!
     */
    public static int loadShader( int type, String shaderCode )
    {
        // create a vertex shader type (GLES20.GL_VERTEX_SHADER)
        // or a fragment shader type (GLES20.GL_FRAGMENT_SHADER)
        int shader = GLES20.glCreateShader( type );

        // add the source code to the shader and compile it
        GLES20.glShaderSource( shader, shaderCode );
        GLES20.glCompileShader( shader );

        return shader;
    }

    /**
     * Adds an ingredient to the pizza.
     *
     * @param ingredient The ingredient to add to the pizza
     */
    public void addIngredient( INGREDIENT ingredient )
    {
        switch( ingredient )
        {
            case CHEESE:
                addCheese();
                break;
            case CAPSICUM:
                addCapsicum();
                break;
            case HAM:
                addHam();
                break;
            case MUSHROOM:
                addMushroom();
                break;
            case OLIVES:
                addOlives();
                break;
            case ONION:
                addOnion();
                break;
            case PEPPERONI:
                addPepperoni();
                break;
            case PINEAPPLE:
                addPineapple();
                break;
            case TOMATOSAUCE:
                addTomatoSauce();
                break;
            default:
                System.out.println( "INGREDIENT added that cannot be drawn - have you added new ingredients recently?" );
        }
    }

    /**
     * Clears the predraw and drawn shapes list.
     */
    public void clearPizza()
    {
        synchronized( infoList )
        {
            infoList.clear();
        }
        synchronized( shapeList )
        {
            shapeList.clear();
        }
    }

    /**
     *
     */
    public void addPepperoni()
    {
        synchronized( infoList )
        {
            float radius = .09F;
            float[] color = {0.571F, 0.188F, 0.0F, 1.0F}; //pepperoni colour

            addToDrawQueue( "circle", radius, radius, getRandomPosition(), color );
            addToDrawQueue( "circle", radius, radius, getRandomPosition(), color );
            addToDrawQueue( "circle", radius, radius, getRandomPosition(), color );
            addToDrawQueue( "circle", radius, radius, getRandomPosition(), color );
            addToDrawQueue( "circle", radius, radius, getRandomPosition(), color );
            addToDrawQueue( "circle", radius, radius, getRandomPosition(), color );
            addToDrawQueue( "circle", radius, radius, getRandomPosition(), color );
            addToDrawQueue( "circle", radius, radius, getRandomPosition(), color );
            addToDrawQueue( "circle", radius, radius, getRandomPosition(), color );
            addToDrawQueue( "circle", radius, radius, getRandomPosition(), color );
            addToDrawQueue( "circle", radius, radius, getRandomPosition(), color );
            addToDrawQueue( "circle", radius, radius, getRandomPosition(), color );
        }
    }

    /**
     *
     */
    public void addTomatoSauce()
    {
        synchronized( infoList )
        {
            addToDrawQueue( "circle", sauceRadius, sauceRadius, new float[]{0F, 0F}, new float[]{1.0F, 0.0F, 0.0F, 1.0F} );
        }
    }

    /**
     *
     */
    public void addHam()
    {
        synchronized( infoList )
        {
            float length = .1F;
            float height = .1F;
            float[] color = {219F / 255F, 158F / 255F, 166F / 255F, 1F}; //pink

            addToDrawQueue( "quadrilateral", length, height, getRandomPosition(), color );
            addToDrawQueue( "quadrilateral", length, height, getRandomPosition(), color );
            addToDrawQueue( "quadrilateral", length, height, getRandomPosition(), color );
            addToDrawQueue( "quadrilateral", length, height, getRandomPosition(), color );
            addToDrawQueue( "quadrilateral", length, height, getRandomPosition(), color );
            addToDrawQueue( "quadrilateral", length, height, getRandomPosition(), color );
            addToDrawQueue( "quadrilateral", length, height, getRandomPosition(), color );
            addToDrawQueue( "quadrilateral", length, height, getRandomPosition(), color );
            addToDrawQueue( "quadrilateral", length, height, getRandomPosition(), color );
            addToDrawQueue( "quadrilateral", length, height, getRandomPosition(), color );
            addToDrawQueue( "quadrilateral", length, height, getRandomPosition(), color );
            addToDrawQueue( "quadrilateral", length, height, getRandomPosition(), color );
        }
    }

    /**
     *
     */
    public void addPineapple()
    {
        synchronized( infoList )
        {
            float base = .1F;
            float height = .1F;
            float color[] = {1.0F, 1.0F, 0.0F, 1.0F}; //yellow

            addToDrawQueue( "triangle", base, height, getRandomPosition(), color );
            addToDrawQueue( "triangle", base, height, getRandomPosition(), color );
            addToDrawQueue( "triangle", base, height, getRandomPosition(), color );
            addToDrawQueue( "triangle", base, height, getRandomPosition(), color );
            addToDrawQueue( "triangle", base, height, getRandomPosition(), color );
            addToDrawQueue( "triangle", base, height, getRandomPosition(), color );
            addToDrawQueue( "triangle", base, height, getRandomPosition(), color );
            addToDrawQueue( "triangle", base, height, getRandomPosition(), color );
            addToDrawQueue( "triangle", base, height, getRandomPosition(), color );
            addToDrawQueue( "triangle", base, height, getRandomPosition(), color );
            addToDrawQueue( "triangle", base, height, getRandomPosition(), color );
            addToDrawQueue( "triangle", base, height, getRandomPosition(), color );
        }
    }

    /**
     *
     */
    public void addOnion()
    {
        synchronized( infoList )
        {
            float size = .05F; //onion is a square shape
            float[] color = {1.0F, 1.0F, 1.0F, 1F}; //white

            addToDrawQueue( "quadrilateral", size, size, getRandomPosition(), color );
            addToDrawQueue( "quadrilateral", size, size, getRandomPosition(), color );
            addToDrawQueue( "quadrilateral", size, size, getRandomPosition(), color );
            addToDrawQueue( "quadrilateral", size, size, getRandomPosition(), color );
            addToDrawQueue( "quadrilateral", size, size, getRandomPosition(), color );
            addToDrawQueue( "quadrilateral", size, size, getRandomPosition(), color );
            addToDrawQueue( "quadrilateral", size, size, getRandomPosition(), color );
            addToDrawQueue( "quadrilateral", size, size, getRandomPosition(), color );
            addToDrawQueue( "quadrilateral", size, size, getRandomPosition(), color );
            addToDrawQueue( "quadrilateral", size, size, getRandomPosition(), color );
            addToDrawQueue( "quadrilateral", size, size, getRandomPosition(), color );
            addToDrawQueue( "quadrilateral", size, size, getRandomPosition(), color );
        }
    }

    /**
     *
     */
    public void addOlives()
    {
        synchronized( infoList )
        {
            float radius = .05F;
            float[] color = {0.0F, 0.0F, 0.0F, 1.0F}; //black

            addToDrawQueue( "circle", radius, radius, getRandomPosition(), color );
            addToDrawQueue( "circle", radius, radius, getRandomPosition(), color );
            addToDrawQueue( "circle", radius, radius, getRandomPosition(), color );
            addToDrawQueue( "circle", radius, radius, getRandomPosition(), color );
            addToDrawQueue( "circle", radius, radius, getRandomPosition(), color );
            addToDrawQueue( "circle", radius, radius, getRandomPosition(), color );
            addToDrawQueue( "circle", radius, radius, getRandomPosition(), color );
            addToDrawQueue( "circle", radius, radius, getRandomPosition(), color );
            addToDrawQueue( "circle", radius, radius, getRandomPosition(), color );
            addToDrawQueue( "circle", radius, radius, getRandomPosition(), color );
            addToDrawQueue( "circle", radius, radius, getRandomPosition(), color );
            addToDrawQueue( "circle", radius, radius, getRandomPosition(), color );
        }
    }

    /**
     *
     */
    public void addMushroom()
    {
        synchronized( infoList )
        {
            float height = .1F;
            float width = .1F;
            float[] color = {230F / 255F, 229F / 255F, 227F / 255, 1.0F}; //grey

            addToDrawQueue( "mushroom", width, height, getRandomPosition(), color );
            addToDrawQueue( "mushroom", width, height, getRandomPosition(), color );
            addToDrawQueue( "mushroom", width, height, getRandomPosition(), color );
            addToDrawQueue( "mushroom", width, height, getRandomPosition(), color );
            addToDrawQueue( "mushroom", width, height, getRandomPosition(), color );
            addToDrawQueue( "mushroom", width, height, getRandomPosition(), color );
            addToDrawQueue( "mushroom", width, height, getRandomPosition(), color );
            addToDrawQueue( "mushroom", width, height, getRandomPosition(), color );
            addToDrawQueue( "mushroom", width, height, getRandomPosition(), color );
        }
    }


    /**
     *
     */
    public void addCheese()
    {
        synchronized( infoList )
        {
            float[] color = {255F / 255F, 196F / 255F, 0F, 1.0F}; //orangey yellow
            addToDrawQueue( "cheese", positionRadius, positionRadius, getRandomPosition(), color );
        }
    }

    /**
     *
     */
    public void addCapsicum()
    {
        synchronized( infoList )
        {
            float height = .1F;
            float width = .075F;
            float[] color = {57F / 255F, 125F / 255F, 2F / 255F, 1.0F}; //"pepper green"

            addToDrawQueue( "capsicum", width, height, getRandomPosition(), color );
            addToDrawQueue( "capsicum", width, height, getRandomPosition(), color );
            addToDrawQueue( "capsicum", width, height, getRandomPosition(), color );
            addToDrawQueue( "capsicum", width, height, getRandomPosition(), color );
            addToDrawQueue( "capsicum", width, height, getRandomPosition(), color );
            addToDrawQueue( "capsicum", width, height, getRandomPosition(), color );
            addToDrawQueue( "capsicum", width, height, getRandomPosition(), color );
            addToDrawQueue( "capsicum", width, height, getRandomPosition(), color );
            addToDrawQueue( "capsicum", width, height, getRandomPosition(), color );
            addToDrawQueue( "capsicum", width, height, getRandomPosition(), color );
            addToDrawQueue( "capsicum", width, height, getRandomPosition(), color );
            addToDrawQueue( "capsicum", width, height, getRandomPosition(), color );
        }
    }

    /**
     *
     */
    private void drawPizzaBase()
    {
        pizzaBase = new Circle( baseRadius, 0, 0, new float[]{0.8275f, 0.7059f, 0.5255f, 1.0f}, new float[16] );
    }

    /**
     *
     *
     * @param shape
     * @param length
     * @param height
     * @param pos
     * @param color
     */
    private void addToDrawQueue( String shape, float length, float height, float[] pos, float[] color )
    {
        infoList.add( new InfoClass( shape, length, height, pos, color ) );
    }

    /**
     * Calculates a random position within the bounds of the pizza sauce
     *
     * @return float array containing a random x & y position
     */
    private float[] getRandomPosition()
    {
        float x = (float) Math.random() * (2F * positionRadius) - positionRadius;
        float yMax = (float) Math.sqrt( positionRadius * positionRadius - x * x );
        float y = (float) Math.random() * 2F * yMax - yMax;

        return new float[]{x, y};
    }
}
