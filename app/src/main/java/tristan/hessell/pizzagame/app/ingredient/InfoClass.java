package tristan.hessell.pizzagame.app.ingredient;

/**
 * Container class for information pertaining to a shape.
 * This is used so shapes can be queued to be drawn and created in the onDraw method
 * (as you can only do OGL stuff in the OGL thread)
 * TODO There may be a better way to do this (without this class)
 * <p/>
 * Created by Tristan on 24/04/2015.
 */
public class InfoClass
{
    private float[] color;
    private float x;
    private float y;
    private float length;
    private float height;
    private String shape;

    public InfoClass( String shapeStr, float length, float height, float x, float y, float[] color )
    {
        shape = shapeStr;
        this.length = length;
        this.height = height;
        this.x = x;
        this.y = y;
        this.color = color;
    }

    public InfoClass( String shapeStr, float length, float height, float[] position, float[] color )
    {
        this( shapeStr, length, height, position[0], position[1], color );
    }

    public String getShape()
    {
        return shape;
    }

    public float getHeight()
    {
        return height;
    }

    public float getLength()
    {
        return length;
    }

    public float getY()
    {
        return y;
    }

    public float getX()
    {
        return x;
    }

    public float[] getColor()
    {
        return color;
    }
}
