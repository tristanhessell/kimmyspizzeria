package tristan.hessell.pizzagame.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import tristan.hessell.pizzagame.app.pizza.PizzaFactory;

/**
 * The title screen that the user first sees when they open the game.
 * Contains the title and the button bar at the bottom of the game.
 */
public class StartActivity extends Activity
{

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_start );
        PizzaFactory.loadPizzas( this.getResources().openRawResource( R.raw.pizzas ) );
    }

    /**
     * Button click that goes to the actual game.
     *
     * @param v required for onClick assigned via XML
     */
    public void btnStartClick( View v )
    {
        Intent intent = new Intent( this, MainActivity.class );
        startActivity( intent );
    }

    /**
     * Button click that goes to the ingredient page.
     *
     * @param v required for onClick assigned via XML
     */
    public void btnIngredientsClick( View v )
    {
        Intent intent = new Intent( this, IngredientsActivity.class );
        startActivity( intent );
    }

    /**
     *Button click that goes to the recipe page.
     *
     * @param v required for onClick assigned via XML
     */
    public void btnRecipesClick( View v )
    {
        Intent intent = new Intent( this, RecipeActivity.class );
        startActivity( intent );
    }

    /**
     * Button click that goes to the tutorial.
     *
     * @param v required for onClick assigned via XML
     */
    public void btnHowToPlayClick( View v )
    {
        Intent intent = new Intent( this, TutorialActivity.class );
        startActivity( intent );
    }

}
