package tristan.hessell.pizzagame.app.ingredient;

import android.opengl.Matrix;

/**
 * Class in charge of creating the PizzaShapes for drawing.
 * <p/>
 * Created by Tristan on 24/04/2015.
 */
public class ShapeCreator
{
    public static PizzaShape getShape( InfoClass info )
    {
        PizzaShape shape;

        switch( info.getShape() )
        {
            case "circle":
                shape = new Circle( info.getLength(), info.getX(), info.getY(), info.getColor(), getRandomRotation() );
                break;
            case "triangle":
                shape = new Triangle( info.getLength(), info.getHeight(), info.getX(), info.getY(), info.getColor(), getRandomRotation() );
                break;
            case "quadrilateral":
                shape = new Rectangle( info.getLength(), info.getHeight(), info.getX(), info.getY(), info.getColor(), getRandomRotation() );
                break;
            case "mushroom":
                shape = new Mushroom( info.getLength(), info.getHeight(), info.getX(), info.getY(), info.getColor(), getRandomRotation() );
                break;
            case "capsicum":
                shape = new Capsicum( info.getLength(), info.getHeight(), info.getX(), info.getY(), info.getColor(), getRandomRotation() );
                break;
            case "cheese": //using length to hold the radius that the cheese can be within.
                shape = new Cheese( info.getLength(), info.getColor() );
                break;
            default:
                throw new IllegalArgumentException( "Invalid shape info" );
        }

        return shape;
    }

    /**
     * Creates a random rotation array, which is required to be a size 16 float array.
     * This represents the 4 x 4 3D rotation matrix.
     *
     * @return Size 16 float array containing a rotation matrix.
     */
    private static float[] getRandomRotation()
    {
        float angle = (float) Math.random() * 360.0F;
        float[] rotMatrix = new float[16];

        Matrix.setIdentityM( rotMatrix, 0 );
        Matrix.translateM( rotMatrix, 0, 0, 0, 0 );
        Matrix.rotateM( rotMatrix, 0, angle, 0.0f, 0.0f, -1.0f );

        return rotMatrix;
    }
}
