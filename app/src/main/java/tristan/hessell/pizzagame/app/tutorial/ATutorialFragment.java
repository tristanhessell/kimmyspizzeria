package tristan.hessell.pizzagame.app.tutorial;

import android.support.v4.app.Fragment;

/**
 * Abstract base class for the tutorials fragments
 * Used so that the title of the tutorial can be used in the Tutorial Activity
 *
 * Created by Tristan on 25/06/2015.
 */
public abstract class ATutorialFragment extends Fragment
{
    public ATutorialFragment()
    {

    }

    public abstract String getName();
}
