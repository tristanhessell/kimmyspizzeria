package tristan.hessell.pizzagame.app.pizza;

import tristan.hessell.pizzagame.app.R;
import tristan.hessell.pizzagame.app.ingredient.INGREDIENT;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

/**
 * Class used to represent the pizza throughout the game.
 * Made as immutable as possible.
 *
 * Created by Tristan on 19/04/2015.
 */
public final class Pizza implements Comparable<Pizza>
{
    /**
     * Enum used to represent the kind of pizza that the pizza object is.
     * Only required internal to the class.
     * Used to ensure the mapping between the pizza and the resource
     */
    private enum PIZZA_NAME
    {
        HAWAIIAN( "Hawaiian", R.drawable.pizza_hawaiian ),
        SUPEREMO( "Superemo", R.drawable.pizza_superemo ),
        VEGGIE( "Veggie", R.drawable.pizza_veggie ),
        PEPPERONI( "Pepperoni", R.drawable.pizza_pepperoni ),
        MARGHERITA( "Margherita", R.drawable.pizza_margherita ),
        TUSCAN( "Tuscan", R.drawable.pizza_tuscan );

        public final String name; // The nice looking name for the pizza
        public final int resId;   // The resource ID for the image of this pizza

        PIZZA_NAME( String inName, int inResId )
        {
            resId = inResId;
            name = inName;
        }
    }

    private final PIZZA_NAME pizza;
    private final List<INGREDIENT> ingredients;

    /**
     * The constructor for a Pizza object.
     *
     * @param inName        The name of the pizza
     * @param inIngredients The list of ingredients that this pizza contains
     * @throws IllegalArgumentException If inName is represents an invalid pizza
     * @throws NullPointerException     If inName is null.
     */
    public Pizza( final String inName, final List<INGREDIENT> inIngredients )
    {
        pizza = PIZZA_NAME.valueOf( inName.toUpperCase() );
        ingredients = inIngredients;
    }

    /**
     * Checks if the supplied ingredients are the same as the ingredients for this pizza.
     *
     * @param inIngredients The list of ingredients to compare to this pizza's ingredients
     * @return true if the ingredients match, otherwise false
     */
    public final boolean ingredientsCorrect( final Collection<INGREDIENT> inIngredients )
    {
        //if the they have the same number of ingredients AND have the same ingredients - they match
        //thank you short circuit evaluation!
        return inIngredients.size() == ingredients.size() && ingredients.containsAll( inIngredients );
    }

    /**
     * Getter for the name of this pizza
     *
     * @return name of the pizza (String).
     */
    public final String getName()
    {
        return pizza.name;
    }

    /**
     * Getter for the ingredients on this pizza.
     *
     * @return A list of the ingredients on the pizza
     */
    public final List<INGREDIENT> getIngredients()
    {
        return ingredients;
    }

    /**
     * Getter for the resource ID for the image of this pizza.
     *
     * @return the resource ID for the pizza image (int)
     */
    public final int getImageResourceId()
    {
        return pizza.resId;
    }

    @Override
    public final int compareTo( Pizza another )
    {
        //comparing by the string as this will ensure aphabetical order
        //comparing by the enum orders in the order of definition
        return this.pizza.name.compareTo( another.pizza.name );
    }
}

