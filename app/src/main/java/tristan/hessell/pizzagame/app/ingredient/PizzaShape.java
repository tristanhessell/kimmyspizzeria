package tristan.hessell.pizzagame.app.ingredient;

import android.opengl.GLES20;

/**
 * Interface for all the different shapes used in the pizza drawing process.
 * <p/>
 * Created by Tristan on 24/04/2015.
 */
public abstract class PizzaShape
{
    //Used by OGLES when drawing - may actually change between shapes, but i didnt need it to
    //required for rendering the verticies of the shape
    // This matrix member variable provides a hook to manipulate
    // the coordinates of the objects that use this vertex shader
    protected final int vertexShader;


    //Used by OGLES when drawing - may actually change between shapes, but i didnt need it to
    //required for rendering the face of the shape with colours/textures
    protected final int fragmentShader;

    private final float[] rotationMatrix;

    /**
     * Constructor.
     *
     * @param rotMatrix Rotation matrix (4x4 matrix used to rotate the shape)
     */
    public PizzaShape( final float[] rotMatrix )
    {
        rotationMatrix = rotMatrix;

        //shader needs to be drawn for each shape, cant just be created once.
        vertexShader = loadShader( GLES20.GL_VERTEX_SHADER,
                "uniform mat4 uMVPMatrix;" +
                "attribute vec4 vPosition;" +
                "void main() {" +
                "  gl_Position = uMVPMatrix * vPosition;" +
                "}" );

        fragmentShader = loadShader( GLES20.GL_FRAGMENT_SHADER,
                "precision mediump float;" +
                "uniform vec4 vColor;" +
                "void main() {" +
                "  gl_FragColor = vColor;" +
                "}" );
    }

    /**
     * Draw is used by OGLES to draw the shape
     *
     * @param mvpMatrix not sure!
     */
    abstract public void draw( final float[] mvpMatrix );

    /**
     * Used so the shape is only rotated once, when it is first created.
     *
     * @return 4x4 Rotation matrix (4x4 matrix used to rotate the shape).
     */
    public final float[] getRotationMatrix()
    {
        return rotationMatrix;
    }

    /**
     * Used by OGLES when drawing shapes.
     *
     * @param type       The kind of shader that is being loaded
     * @param shaderCode C code which has some sort of meaning
     * @return
     */
    private static int loadShader( final int type, String shaderCode )
    {
        final int shader = GLES20.glCreateShader( type );
        GLES20.glShaderSource( shader, shaderCode );
        GLES20.glCompileShader( shader );
        return shader;
    }
}
