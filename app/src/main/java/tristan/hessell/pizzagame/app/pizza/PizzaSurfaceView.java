package tristan.hessell.pizzagame.app.pizza;

import android.content.Context;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import tristan.hessell.pizzagame.app.ingredient.INGREDIENT;

/**
 * This class is what is used as the canvas to draw everything onto.
 * The renderer actually does most the work, as such this class
 * just wraps most of its [the render's] functionality.
 *
 * All calls the the render should involve the queueEvent() function
 *
 * Created by Tristan on 24/04/2015.
 */
public class PizzaSurfaceView extends GLSurfaceView
{
    private PizzaRenderer renderer;

    /**
     * Constructor for when the surface view is added via XML
     *
     * @param context The activity which the view resides in
     * @param attrs   The various settings that could get applied when added to XML
     */
    public PizzaSurfaceView( final Context context, final AttributeSet attrs )
    {
        super( context, attrs );
        initialiseSurfaceView();
    }

    /**
     * Constructor for when the surface view is added programmatically
     *
     * @param context The activity which the view resides in
     */
    public PizzaSurfaceView( Context context )
    {
        super( context );
        initialiseSurfaceView();
    }

    /**
     * Intialises the settings required to the surface view.
     */
    private void initialiseSurfaceView()
    {
        //required for the transparent background - not entirely sure how it works
        setZOrderOnTop( true );
        setEGLContextClientVersion( 2 );
        setEGLConfigChooser( 8, 8, 8, 8, 16, 0 );
        getHolder().setFormat( PixelFormat.TRANSLUCENT );
        ///////////////

        renderer = new PizzaRenderer();
        setRenderer( renderer );

        //prevents the frame from being redrawn until you call requestRender() (efficiency)
        setRenderMode( GLSurfaceView.RENDERMODE_WHEN_DIRTY );
    }

    /**
     * Adds an ingredient to the pizza
     *
     * @param ingredient The ingredient to be added to the pizza
     */
    public final void addIngredient( final INGREDIENT ingredient )
    {
//        queueEvent( new Runnable()
//        {
//            @Override
//            public void run()
//            {
                renderer.addIngredient( ingredient );
                requestRender();
//            }
//        } );
    }

    /**
     * Adds a collection of ingredients to the pizza.
     *
     * @param ingredients An iterable collection of ingredients that will be added to the pizza
     */
    public final void addIngredients( final Iterable<INGREDIENT> ingredients )
    {
//        queueEvent( new Runnable()
//        {
//            @Override
//            public void run()
//            {
                for( INGREDIENT ing : ingredients )
                {
                    renderer.addIngredient( ing );
                }
                requestRender();
//            }
//        } );


    }

    /**
     * Wrapper function to the renderer; clears the surface view so only the pizza base remains.
     */
    public final void clearPizza()
    {
        queueEvent( new Runnable()
        {
            @Override
            public void run()
            {
                renderer.clearPizza();
                requestRender();
            }
        } );

    }
}
