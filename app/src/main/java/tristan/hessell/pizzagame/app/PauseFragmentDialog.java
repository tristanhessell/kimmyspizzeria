package tristan.hessell.pizzagame.app;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

/**
 * The pause menu for the game; Contains two buttons - continue the game or quit.
 * Created by Tristan on 8/06/2015.
 */
public class PauseFragmentDialog extends DialogFragment
{
    @Override
    public Dialog onCreateDialog( Bundle savedInstanceState )
    {
        final Dialog dialog = super.onCreateDialog( savedInstanceState );

        dialog.getWindow().requestFeature( Window.FEATURE_NO_TITLE ); //removes the title from the top of the dialog

        return dialog;
    }

    @Override
    public View onCreateView( final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState )
    {
        final View v = inflater.inflate( R.layout.pause_fragment, container, false );

        setCancelable( false ); //makes it so that clicking off the dialog doesnt close the dialog

        v.findViewById( R.id.btnContinue ).setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                dismiss();
                ((MainActivity) getActivity()).startTimer();
            }
        } );

        /*v.findViewById( R.id.btnIngredients ).setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                Intent intent = new Intent( getActivity(), IngredientsActivity.class );
                startActivity( intent );
            }
        } );*/

        v.findViewById( R.id.btnRecipes ).setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                Intent intent = new Intent( getActivity(), RecipeActivity.class );
                startActivity( intent );
            }
        } );

        v.findViewById( R.id.btnHowTo ).setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                Intent intent = new Intent( getActivity(), TutorialActivity.class );
                startActivity( intent );
            }
        } );

        v.findViewById( R.id.btnQuit ).setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                getActivity().finish();
            }
        } );

        return v;
    }
}