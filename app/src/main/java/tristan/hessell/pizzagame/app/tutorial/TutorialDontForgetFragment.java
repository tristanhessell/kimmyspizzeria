package tristan.hessell.pizzagame.app.tutorial;

import android.app.Fragment;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import tristan.hessell.pizzagame.app.R;
import tristan.hessell.pizzagame.app.ingredient.INGREDIENT;
import tristan.hessell.pizzagame.app.pizza.PizzaSurfaceView;

import java.util.ArrayList;
import java.util.List;

/**
 * Tutorial Fragment reminding the user that while the order of the ingredients doesn't matter,
 * the amount of ingredients does.
 */
public class TutorialDontForgetFragment extends ATutorialFragment
{
    public TutorialDontForgetFragment()
    {
    }

    @Override
    public String getName()
    {
        return "A Caveat";
    }

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState )
    {
        View v = inflater.inflate( R.layout.fragment_dont_forget_tutorial, container, false );

        ((TextView) v.findViewById( R.id.tvContent )).setText( Html.fromHtml( getString( R.string.tutorial_final_message ) ) );

        PizzaSurfaceView mGLView = (PizzaSurfaceView) v.findViewById( R.id.surfaceview );

        //Using an ingredient list as the renderer will be called less.
        List<INGREDIENT> ingreds = new ArrayList<>( 4 );
        ingreds.add( INGREDIENT.TOMATOSAUCE );
        ingreds.add( INGREDIENT.HAM );
        ingreds.add( INGREDIENT.PINEAPPLE );
        ingreds.add( INGREDIENT.CHEESE );

        mGLView.addIngredients( ingreds );
        return v;
    }
}
