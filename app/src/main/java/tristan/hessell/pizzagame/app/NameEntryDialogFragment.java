package tristan.hessell.pizzagame.app;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 *
 * //TODO add in onPause stuff so that the editText value is kept when the screen is paused
 */
public class NameEntryDialogFragment extends DialogFragment
{
    private static final String TITLE_IDENTIFIER    = "title";
    private static final String CALLBACK_IDENTIFIER = "score";
    private static final String MESSAGE_IDENTIFIER  = "message";
    //private static final String NAME_IDENTIFIER     = "name";

    private DialogCallback cb;
    private String title;
    private String message;
    private EditText etName;
    //private String name;

    public static NameEntryDialogFragment newInstance(final String inTitle, final DialogCallback inCb)
    {
        NameEntryDialogFragment frag = new NameEntryDialogFragment();
        Bundle args = new Bundle();
        args.putString( TITLE_IDENTIFIER, inTitle );
        args.putParcelable( CALLBACK_IDENTIFIER, inCb );
        frag.setArguments( args );

        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        getValuesFromArguments( getArguments() );

        setCancelable( false ); //makes it so that clicking off the dialog doesnt close the dialog
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate( R.layout.dialog_string_entry, null );

        etName = (EditText)v.findViewById( R.id.edittext );
        //etName.setText( name );
        ( (TextView)v.findViewById( R.id.dlgbody ) ).setText( message );

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle( title )
            .setView( v )
            .setPositiveButton( "OK",
                    new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick( DialogInterface dialog, int whichButton )
                        {
                            cb.onCallback( etName.getText().toString() );
                        }
                    } );
        return builder.create();
    }

    //if a value isnt present, a default is used
    //Title, callback and the list of strings are required - exception will be thrown if they are not present
    private void getValuesFromArguments(Bundle args)
    {
        if(args == null)
        {
            throw new IllegalArgumentException( "Cannot create Dialog: No arguments passed to dialog" );
        }

        title = args.getString( TITLE_IDENTIFIER );
        if(title == null)
        {
            throw new IllegalArgumentException( "Cannot create Dialog: No Title passed via arguments" );
        }

        cb = args.getParcelable( CALLBACK_IDENTIFIER );
        if(cb == null)
        {
            throw new IllegalArgumentException( "Cannot create Dialog: No callback passed via arguments" );
        }

        //message will be empty if none is specified
        message = args.getString( MESSAGE_IDENTIFIER, "" );
        //name    = args.getString( NAME_IDENTIFIER, "" );
    }

    public final NameEntryDialogFragment setMessage( final String s )
    {
        getArguments().putString( MESSAGE_IDENTIFIER, s );

        return this;
    }
}
