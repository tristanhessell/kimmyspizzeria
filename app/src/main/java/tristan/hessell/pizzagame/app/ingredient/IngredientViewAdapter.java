package tristan.hessell.pizzagame.app.ingredient;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;

/**
 * Adapter for the Ingredients. This allows the list of IngredientView to be used with a GridView.
 * <p/>
 * Created by Tristan on 20/05/2015.
 */
public class IngredientViewAdapter extends BaseAdapter
{
    private final Context context;

    public IngredientViewAdapter( final Context c )
    {
        context = c;
    }

    @Override
    public int getCount()
    {
        return INGREDIENT.values.length;
    }

    @Override
    public Object getItem( final int position )
    {
        return null;
    }

    @Override
    public long getItemId( final int position )
    {
        return position;
    }

    @Override
    public View getView( final int position, final View convertView, final ViewGroup parent )
    {
        final IngredientView ingView;

        //This block is required for a GridView, where there is only a small number are actually created,
        //instead the contents of the view are updates when required.
        if( convertView == null )
        {
            ingView = new IngredientView( context, INGREDIENT.values[position] );
            ingView.setLayoutParams( new GridView.LayoutParams( GridView.LayoutParams.MATCH_PARENT, GridView.LayoutParams.WRAP_CONTENT ) );
        } else
        {
            ingView = (IngredientView) convertView;
            ingView.setIngredient( INGREDIENT.values[position] );
        }

        return ingView;
    }
}
