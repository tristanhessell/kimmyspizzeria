package tristan.hessell.pizzagame.app.ingredient;

import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Class responsible for a Mushroom shape to be drawn using OGL20.
 * <p/>
 * Created by Tristan on 24/04/2015.
 */
public class Mushroom extends PizzaShape
{
    private int mProgram;
    private FloatBuffer mVertexBuffer;
    private final float color[];
    //TODO what was 186 exactly for?
    private final int NumPoints = 186;

    public Mushroom( float width, float height, float x, float y, float[] inColor, float[] inRot )
    {
        super( inRot );
        color = inColor;

        final float vertices[] = new float[(NumPoints) * 3];

        vertices[0] = x;
        vertices[1] = y;
        vertices[2] = 0;

        for( int i = 1; i <= 181; i++ )
        {
            vertices[(i * 3)] = (x + (float) (Math.cos( (3.14 / 180) * (float) (i - 1) )) * height);
            vertices[(i * 3) + 1] = (y + (float) (Math.sin( (3.14 / 180) * (float) (i - 1) )) * width);
            vertices[(i * 3) + 2] = 0;
        }

        vertices[546] = x + (-0.4F * height);
        vertices[547] = y + (0F * width);
        vertices[548] = 0F;

        vertices[549] = x + (-0.4F * height);
        vertices[550] = y + (-0.5F * width);
        vertices[551] = 0F;

        vertices[552] = x + (0.4F * height);
        vertices[553] = y + (-0.5F * width);
        vertices[554] = 0;

        vertices[555] = x + (0.4F * height);
        vertices[556] = y + (0F * width);
        vertices[557] = 0;

        final ByteBuffer vertexByteBuffer = ByteBuffer.allocateDirect( vertices.length * 4 );
        vertexByteBuffer.order( ByteOrder.nativeOrder() );
        mVertexBuffer = vertexByteBuffer.asFloatBuffer();
        mVertexBuffer.put( vertices );
        mVertexBuffer.position( 0 );

        mProgram = GLES20.glCreateProgram();             // create empty OpenGL ES Program
        GLES20.glAttachShader( mProgram, vertexShader );   // add the vertex shader to program
        GLES20.glAttachShader( mProgram, fragmentShader ); // add the fragment shader to program
        GLES20.glLinkProgram( mProgram );
    }

    @Override
    public void draw( final float[] mvpMatrix )
    {
        GLES20.glUseProgram( mProgram );

        // get handle to vertex shader's vPosition member
        final int mPositionHandle = GLES20.glGetAttribLocation( mProgram, "vPosition" );

        // Enable a handle to the triangle vertices
        GLES20.glEnableVertexAttribArray( mPositionHandle );

        // Prepare the triangle coordinate data
        GLES20.glVertexAttribPointer( mPositionHandle, 3, GLES20.GL_FLOAT, false, 12, mVertexBuffer );

        // get handle to fragment shader's vColor member
        final int mColorHandle = GLES20.glGetUniformLocation( mProgram, "vColor" );

        // Set color for drawing the triangle
        GLES20.glUniform4fv( mColorHandle, 1, color, 0 );

        final int mMVPMatrixHandle = GLES20.glGetUniformLocation( mProgram, "uMVPMatrix" );

        // Apply the projection and view transformation
        GLES20.glUniformMatrix4fv( mMVPMatrixHandle, 1, false, mvpMatrix, 0 );

        // Draw the triangle
        GLES20.glDrawArrays( GLES20.GL_TRIANGLE_FAN, 0, NumPoints );

        // Disable vertex array
        GLES20.glDisableVertexAttribArray( mPositionHandle );
    }

}
