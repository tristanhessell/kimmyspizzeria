package tristan.hessell.pizzagame.app.tutorial;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import tristan.hessell.pizzagame.app.R;
import tristan.hessell.pizzagame.app.ingredient.INGREDIENT;
import tristan.hessell.pizzagame.app.ingredient.IngredientButton;
import tristan.hessell.pizzagame.app.pizza.PizzaSurfaceView;

import java.util.ArrayList;

public class TutorialAddIngredientsFragment extends ATutorialFragment
{
    private static final String INGREDIENTS_CODE = "ingredients";
    private PizzaSurfaceView mGLView;
    private ArrayList<INGREDIENT> ingredients = new ArrayList<>();

    public TutorialAddIngredientsFragment()
    {}

    @Override
    public String getName()
    {
        return "Adding Ingredients";
    }

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
    }

    @Override
    public void onSaveInstanceState( Bundle outState )
    {
        super.onSaveInstanceState( outState );
        outState.putParcelableArrayList( INGREDIENTS_CODE, ingredients );
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState )
    {
        View v = inflater.inflate( R.layout.fragment_add_ingredients_tutorial, container, false );

        ((TextView) v.findViewById( R.id.tvContent )).setText( Html.fromHtml( getString( R.string.tutorial_add_ingredients ) ) );
        mGLView = (PizzaSurfaceView) v.findViewById( R.id.surfaceview );

        v.findViewById( R.id.btnCheese ).setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                onIngredientButtonClick( v );
            }
        } );
        v.findViewById( R.id.btnHam ).setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                onIngredientButtonClick( v );
            }
        } );
        v.findViewById( R.id.btnTom ).setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                onIngredientButtonClick( v );
            }
        } );

        v.findViewById( R.id.btnCaps ).setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                onIngredientButtonClick( v );
            }
        } );
        v.findViewById( R.id.btnPepperoni ).setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                onIngredientButtonClick( v );
            }
        } );
        v.findViewById( R.id.btnMushroom ).setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                onIngredientButtonClick( v );
            }
        } );

        if( savedInstanceState != null )
        {
            ingredients = savedInstanceState.getParcelableArrayList( INGREDIENTS_CODE );
            mGLView.addIngredients( ingredients );
        }

        return v;
    }

    /**
     * Clicking on an ingredient adds the ingredient to the surfaceview.
     *
     * @param v Required for XML assigning onClick
     */
    public void onIngredientButtonClick( View v )
    {
        INGREDIENT ing = ((IngredientButton) v).getIngredient();
        ingredients.add( ing );
        mGLView.addIngredient( ing );
    }

}
