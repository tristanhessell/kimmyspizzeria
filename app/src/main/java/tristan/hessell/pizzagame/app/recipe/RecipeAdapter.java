package tristan.hessell.pizzagame.app.recipe;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import tristan.hessell.pizzagame.app.pizza.Pizza;
import tristan.hessell.pizzagame.app.pizza.PizzaFactory;

import java.util.List;

/**
 * Adapter for the Recipes. This allows the list of Recipes to be used with a GridView
 *
 * Created by Tristan on 20/05/2015.
 */
public class RecipeAdapter extends BaseAdapter
{
    private final Context context;
    private final List<Pizza> pizzas;

    public RecipeAdapter( final Context c )
    {
        context = c;
        pizzas = PizzaFactory.getAllPizzas();
    }

    @Override
    public int getCount()
    {
        return pizzas.size();
    }

    @Override
    public Object getItem( final int position )
    {
        return null;
    }

    @Override
    public long getItemId( final int position )
    {
        return position;
    }

    @Override
    public View getView( final int position, final View convertView, final ViewGroup parent )
    {
        final RecipeView recipe;

        //This block is required for a GridView, where there is only a small number are actually created,
        //instead the contents of the view are updates when required.
        if( convertView == null )
        {
            recipe = new RecipeView( context, pizzas.get( position ) );
            recipe.setLayoutParams( new GridView.LayoutParams( GridView.LayoutParams.MATCH_PARENT, GridView.LayoutParams.WRAP_CONTENT ) );
        } else
        {
            recipe = (RecipeView) convertView;
            recipe.setPizza( pizzas.get( position ) );
        }

        return recipe;
    }
}
